<?php
/**
 * Created by PhpStorm.
 * User: x0st
 * Date: 12.08.16
 * Time: 17:51
 */

namespace app\assets;


use yii\web\AssetBundle;

class ListTableAsset extends AssetBundle
{
    public $js = [
        'vendor/bower/avega-yii2-list-table/avega.yii2-list-table.js'
    ];

    public $css = [
        'vendor/bower/avega-yii2-list-table/avega.yii2-list-table.css'
    ];

    public $depends = [
        'app\assets\BsAlertAsset',
        'app\assets\JqueryAsset'
    ];
}