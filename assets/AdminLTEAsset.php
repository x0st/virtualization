<?php
/**
 * Created by PhpStorm.
 * User: x0st
 * Date: 12.08.16
 * Time: 17:47
 */

namespace app\assets;


use yii\web\AssetBundle;

class AdminLTEAsset extends AssetBundle
{
    public $css = [
        'vendor/bower/AdminLTE/dist/css/AdminLTE.min.css',
        'vendor/bower/AdminLTE/dist/css/skins/_all-skins.css'
    ];

    public $js = [
        'vendor/bower/AdminLTE/dist/js/app.min.js'
    ];

    public $depends = [
        'app\assets\JqueryAsset'
    ];
}