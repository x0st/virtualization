<?php
/**
 * Created by PhpStorm.
 * User: x0st
 * Date: 12.08.16
 * Time: 17:49
 */

namespace app\assets;

use yii\web\AssetBundle;

class FormAsset extends AssetBundle
{
    public $js = [
        'vendor\bower\avega-yii2-form\avega.yii2-form.js'
    ];

    public $css = [
        'vendor\bower\avega-yii2-form\avega.yii2-form.css'
    ];

    public $depends = [
        'app\assets\BsAlertAsset',
        'app\assets\JqueryAsset'
    ];
}