<?php
/**
 * Created by PhpStorm.
 * User: x0st
 * Date: 12.08.16
 * Time: 18:46
 */

namespace app\assets;


use yii\web\AssetBundle;
use yii\web\View;

class EditableTableAsset extends AssetBundle
{
    public $jsOptions = [
        'position' => View::POS_HEAD
    ];

    public $js = [
        'vendor/bower/avega-yii2-editable-table/avega.yii2-editable-table.js'
    ];

    public $css = [
        'vendor/bower/avega-yii2-editable-table/avega.yii2-editable-table.css'
    ];
}