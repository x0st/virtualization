<?php
/**
 * Created by PhpStorm.
 * User: x0st
 * Date: 12.08.16
 * Time: 17:48
 */

namespace app\assets;


use yii\web\AssetBundle;

class JqueryAsset extends AssetBundle
{
    public $js = [
        'vendor/bower/jquery/dist/jquery.js'
    ];
}