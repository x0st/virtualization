<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 28.08.16
 * Time: 00:44
 */

namespace app\assets;


use yii\web\AssetBundle;

class BsAlertAsset extends AssetBundle
{
    public $js = [
        'vendor/bower/bs-alert/bs.alert.js'
    ];

    public $depends = [
        'app\assets\JqueryAsset'
    ];
}