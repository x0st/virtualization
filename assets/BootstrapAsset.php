<?php
/**
 * Created by PhpStorm.
 * User: x0st
 * Date: 12.08.16
 * Time: 18:47
 */

namespace app\assets;


use yii\web\AssetBundle;

class BootstrapAsset extends AssetBundle
{
    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];

    public $js = [
        'vendor/bower/bootstrap/dist/js/bootstrap.js'
    ];

    public $css = [
        'vendor/bower/bootstrap/dist/css/bootstrap.css'
    ];

    public $depends = [
        'app\assets\JqueryAsset'
    ];
}