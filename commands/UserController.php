<?php
/**
 * Created by PhpStorm.
 * User: x0st
 * Date: 12.08.16
 * Time: 22:18
 */

namespace app\commands;


use app\models\User;
use yii\console\Controller;

class UserController extends Controller
{
    public function actionCreateAdmin($username, $password)
    {
        $user = new User();

        $user->name = 'Admin';
        $user->username = $username;
        $user->setPassword($password);
        $user->generateAuthKey();

        echo $user->save() ? 'User has been created' . "\n" : 'Creating error' . "\n";
    }
}