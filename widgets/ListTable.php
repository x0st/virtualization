<?php
/**
 * Created by PhpStorm.
 * User: Pavel
 * Date: 5/5/2016
 * Time: 9:33 AM
 */

namespace app\widgets;

use yii\base\Widget;

class ListTable extends Widget
{
	public $id;

	public $columns;

	public $pagination;

	private $result = '';

	public function init()
	{
		if ( $this->id !== null && $this->columns !== null && count($this->columns) > 0 ) {
			$this->result = '<table id="' . $this->id . '" class="table table-bordered table-striped">';
			$this->result .= '<thead><tr>';

			foreach ($this->columns as $column => $attributes) {
				$item = $column;
				$class = '';
				$title = '';
				
				if ($column == 'actions') {
					if ($attributes === true) {
						$th = '<th class="list-table-actions">' . 'Actions' . '</th>';
					}else{
						continue;
					}
				} else {
					$none_sort = false;

					foreach ($attributes as $k => $attribute) {
						if ($k === 0) {
							$title = $attribute;
						}

						if ($k === 'class') {
							$class .= ' ' . $attribute . ' ';
						}

						if ($attribute === 'mob') {
							$class .= ' visible-mobile-vertical ';
						}

						if ($attribute === 'none') {
							$class .= ' none-sort ';
							$none_sort = true;
						}

						if ($attribute === 'desktop') {
							$class .= ' visible-desktop ';
						}
					}

					if (!$none_sort) {
						$class .= ' sort sorting ';
					}

					$th = '<th data-column-name="' . $item . '" class="' . $class . '">' . $title . '</th>';

				}

				$this->result .= $th;
			}

			$this->result .= '</tr></thead><tbody></tbody></table>';

			if ( $this->pagination !== null ) {
				if ( $this->pagination === true ) {
					$this->result .= '<div class="row pagination-data"></div>';
				} else {
					$this->result .= '<div class="' . $this->pagination . '"></div>';
				}
			}
		}
	}

	public function run()
	{
		return ($this->result);
	}
}