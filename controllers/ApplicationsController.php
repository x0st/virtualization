<?php
/**
 * Создание, удаление, обновление приложений.
 * Created by PhpStorm.
 * User: x0st
 * Date: 13.08.16
 * Time: 22:43
 */

namespace app\controllers;

use app\models\Application;
use app\models\ApplicationClone;
use app\models\ApplicationForm;
use app\models\ApplicationList;
use app\models\ContainerSection;
use app\models\Image;
use app\models\UploadFileForm;
use avega\F;
use ErrorException;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

class ApplicationsController extends CommonController
{
    private $response = ['status' => 0];

    /**
     * Страница просмотра всех приложений.
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Возвращает массив приложений с пагинацией (ajax).
     */
    public function actionGetList()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        return ApplicationList::getList();
    }

    /**
     * Страница создания нового приложения.
     */
    public function actionNew()
    {
        $images = Image::find()
            ->with('branches')
            ->all();

        return $this->render('new', [
            'images' => $images
        ]);
    }

    /**
     * Клонирует приложение со всеми его контейнерами.
     */
    public function actionClone($id)
    {
        if (Application::find()->where(['id' => $id])->exists()) {
            $appClone = new ApplicationClone($id);
            $appId = $appClone->cloneApp();

            if ($appId !== null) {
                return $this->redirect(Url::to(['/applications/edit/' . $appId]));
            } else {
                throw new BadRequestHttpException();
            }
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * Помечает приложение как приложение которое требует перезагрузки (ajax).
     */
    public function actionRestart($id)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $app = Application::findOne($id);

        if ($app !== null) {
            $app->restart = 1;
            $this->response['status'] = (int)$app->save(false);
        } else {
            throw new NotFoundHttpException();
        }

        return $this->response;
    }

    /**
     * Возвращает файл (архив) из папки uploaded-archives.
     */
    public function actionDownloadArchive($file)
    {
        if (!isset(\Yii::$app->params['pathForArchives']) || !is_dir(\Yii::getAlias(\Yii::$app->params['pathForArchives']))) {
            throw new ErrorException('The directory for uploaded files does not exist or was not specified.');
        }

        if (!is_file(\Yii::getAlias(\Yii::$app->params['pathForArchives'] . $file))) {
            throw new NotFoundHttpException('File does not exist.');
        }

        return \Yii::$app->response->sendFile(\Yii::getAlias(\Yii::$app->params['pathForArchives'] . $file));
    }

    /**
     * Загружает файл (архив) для таблицы container_section (ajax).
     */
    public function actionUploadArchive()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new UploadFileForm();
        $model->file = UploadedFile::getInstanceByName('file');

        if ($model->validate() && $model->save()) {
            $this->response = ['status' => 1, 'fileName' => $model->fileName, 'message' => 'File has been uploaded.'];
        } else {
            $this->response['message'] = 'Error of file uploading.';
        }

        return $this->response;
    }

    /**
     * Страница редактирования приложения.
     */
    public function actionEdit($id)
    {
        $app = Application::find()
            ->where(['application.id' => $id])
            ->with('containers.ports')
            ->with('containers.links')
            ->with('containers.sections')
            ->with('containers.environment')
            ->with('containers.externalLinks')
            ->with('containers.volumesFrom')
            ->with('containers.dependencies')
            ->one();

        $images = Image::find()
            ->with('branches')
            ->all();

        if ($app !== null) {
            return $this->render('edit', [
                'application' => $app,
                'images' => $images
            ]);
        } else {
            throw new NotFoundHttpException();
        }

    }

    /**
     * Страница просмотра лога приложения.
     */
    public function actionLog($id)
    {
        $app = Application::findOne($id);

        if ($app !== null) {
            return $this->render('log', ['application' => $app]);
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * Удаляет приложение.
     */
    public function actionDelete($id)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $model = Application::findOne($id);

        if ($model !== null) {
            if ($model->delete()) {
                $this->response = ['status' => 1, 'messages' => '«' . $model->name . '» application has been deleted.'];
            } else {
                throw new BadRequestHttpException();
            }
        } else {
            throw new NotFoundHttpException();
        }

        return $this->response;
    }

    /**
     * Обновляет или созданет новое приложение.
     * @param null $id
     * @return array
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function actionSave($id = null)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        if ($id == null) {
            $model = new ApplicationForm(['scenario' => ApplicationForm::SCENARIO_CREATE]);
        } else {
            $model = ApplicationForm::findOne($id);

            if ($model === null) {
                throw new NotFoundHttpException();
            } else {
                $model->setScenario(ApplicationForm::SCENARIO_UPDATE);
            }
        }

        $transaction = $model::getDb()->beginTransaction();

        try {
            if ($model->load(\Yii::$app->request->post(), 'form') && $model->save()) {
                $this->response['status'] = 1;

                if ($model->scenario === $model::SCENARIO_CREATE) {
                    $this->response['messages'] = '«' . $model->name . '» application has been created.';
                } else {
                    $this->response['messages'] = 'Application has been updated.';
                    $this->response['updates'] = $model->containersTablesIds;
                    $this->response['containersIds'] = $model->containersIds;
                }

                $transaction->commit();
            } elseif ($model->hasErrors()) {
                $this->response['messages'] = $model->errors;
            } else {
                throw new BadRequestHttpException();
            }
        } catch (\Exception $err) {
            $transaction->rollBack();

            throw new BadRequestHttpException();
        }

        return $this->response;
    }
}