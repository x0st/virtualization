<?php
/**
 * Страница со всеми образами, удаление, обновление, создание.
 * Created by PhpStorm.
 * User: x0st
 * Date: 13.08.16
 * Time: 02:36
 */

namespace app\controllers;


use app\models\ApplicationContainer;
use app\models\Image;
use app\models\ImageBranch;
use app\models\ImageForm;
use app\models\ImageList;
use avega\F;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class ImagesController extends CommonController
{
    private $response = ['status' => 0];

    /**
     * Страница со списком всех образов.
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Удаляет образ (ajax).
     */
    public function actionDelete($id)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $image = Image::findOne($id);

        if ($image !== null) {
            if (count(ApplicationContainer::findAll(['image_id' => $image->id])) > 0) {
                $this->response['messages'] = 'You cannot delete the image because this one is used by a container.';
            } elseif ($image->delete()) {
                $this->response = ['messages' => '«' . $image->name . '» image has been deleted.', 'status' => 1];
            } else {
                throw new BadRequestHttpException();
            }
        } else {
            throw new NotFoundHttpException();
        }

        return $this->response;
    }

    /**
     * Возвращает список образов с пагинацией (ajax).
     */
    public function actionGetList()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        return ImageList::getList();
    }

    /**
     * Страница создания нового образа.
     */
    public function actionNew()
    {
        return $this->render('new');
    }

    /**
     * Страница редактирования образа.
     */
    public function actionEdit($id)
    {
        $image = Image::findOne($id);

        if ($image !== null) {
            return $this->render('edit', [
                'image' => $image,
                'branches' => ImageBranch::findAll(['image_id' => $id])
            ]);
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * Создает или обновляет образ и его ветки (ajax).
     */
    public function actionSave($id = null)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        if ($id === null) {
            $model = new ImageForm();
        } else {
            $model = ImageForm::findOne($id);

            if ($model === null) {
                throw new NotFoundHttpException();
            }
        }

        // транзакция потому что еще обновляются ветки образа
        $transaction = $model::getDb()->beginTransaction();

        try {

            if ($model->load(\Yii::$app->request->post(), 'form') && $model->save()) {
                $this->response['status'] = 1;

                if ($id === null) {
                    $this->response['messages'] = '«' . $model->name . '» image has been created.';
                } else {
                    $this->response['messages'] = 'Image has been updated.';
                    $this->response['updates'] = $model->branchesIds;
                }

                $transaction->commit();

            } elseif ($model->hasErrors()) {
                $this->response['messages'] = $model->errors;
            } else {
                throw new BadRequestHttpException();
            }
        } catch (\Exception $err) {
            $transaction->rollBack();

            throw new BadRequestHttpException();
        }

        return $this->response;
    }
}