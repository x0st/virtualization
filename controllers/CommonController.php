<?php
/**
 * Created by PhpStorm.
 * User: x0st
 * Date: 13.08.16
 * Time: 00:03
 */

namespace app\controllers;


use yii\web\Controller;

/**
 * Class CommonController
 * @package app\controllers
 */
class CommonController extends Controller
{
    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if (\Yii::$app->user->isGuest) {
            $this->goHome()->send();
            return false;
        }

        $this->enableCsrfValidation = false;

        return parent::beforeAction($action); // TODO: Change the autogenerated stub
    }
}