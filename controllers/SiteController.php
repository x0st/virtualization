<?php

namespace app\controllers;

use avega\F;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use app\models\LoginForm;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        $this->layout = 'guest';

        if (!Yii::$app->user->isGuest) {
            return $this->redirect(Url::to(['/users']));
        }

        if (Yii::$app->request->isPost) {

            $model = new LoginForm();

            if ($model->load(Yii::$app->request->post(), 'form') && $model->login()) {
                return $this->redirect(Url::to(['/users']));
            } else {
                Yii::$app->session->setFlash('LoginError', 'Wrong login or password.');

                return $this->redirect(Url::to(['/login']));
            }
        }

        return $this->render('login');
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
