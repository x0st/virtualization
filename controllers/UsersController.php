<?php
/**
 * Страница просмотра юзеров, удаление юзера, создание, обновление.
 * Created by PhpStorm.
 * User: x0st
 * Date: 12.08.16
 * Time: 21:36
 */

namespace app\controllers;


use app\models\User;
use app\models\UserForm;
use app\models\UserList;
use avega\F;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class UsersController extends CommonController
{
    private $response = ['status' => 0];

    /**
     * Страница просмотра юзеров.
     */
    public function actionIndex()
    {

        return $this->render('index');
    }

    /**
     * Возвращает список юзеров с пагинацией (ajax).
     */
    public function actionGetList()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        return UserList::getList();
    }

    /**
     * Удаляет юзера (ajax).
     */
    public function actionDelete($id)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        if ($id !== \Yii::$app->user->getId()) {
            $user = User::findOne($id);

            if ($user !== null) {
                if ($user->delete()) {
                    $this->response = ['status' => 1, 'messages' => '«' . $user->name . '» user has been deleted.'];
                } else {
                    $this->response['messages'] = 'Deleting error';
                }
            } else {
                throw new NotFoundHttpException();
            }
        } else {
            throw new BadRequestHttpException();
        }

        return $this->response;
    }

    /**
     * Обновляет или создает нового юзера (ajax).
     */
    public function actionSave($id = null)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        if ($id == null) {
            $model = new UserForm(['scenario' => UserForm::SCENARIO_CREATE]);
        } else {
            $model = UserForm::findOne($id);

            if ($model === null) {
                throw new NotFoundHttpException();
            } else {
                $model->setScenario(UserForm::SCENARIO_UPDATE);
            }
        }

        if ($model->load(\Yii::$app->request->post(), 'form') && $model->save()) {
            $this->response['status'] = 1;

            if ($model->scenario === $model::SCENARIO_UPDATE) {
                $this->response['messages'] = 'User has been updated.';
            } else {
                $this->response['messages'] = '«' . $model->name . '» user has been created.';
            }
        } elseif ($model->hasErrors()) {
            $this->response['messages'] = $model->errors;
        } else {
            throw new BadRequestHttpException();
        }

        return $this->response;
    }

    /**
     * Страница создания нового юзера.
     */
    public function actionNew()
    {
        return $this->render('new');
    }

    /**
     * Страница редактирования юзера.
     */
    public function actionEdit($id)
    {
        $user = User::findOne($id);

        if ($user !== null) {
            return $this->render('edit', ['user' => $user]);
        } else {
            throw new NotFoundHttpException();
        }
    }
}