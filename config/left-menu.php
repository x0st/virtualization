<?php
/**
 * Created by PhpStorm.
 * User: x0st
 * Date: 12.08.16
 * Time: 23:07
 */

$leftMenu = [
    [
        'icon' => 'fa-user',
        'title' => 'Users',
        'page' => '/users'
    ],
    [
        'icon' => 'fa-list',
        'title' => 'Applications',
        'page' => '/applications'
    ],
    [
        'icon' => 'fa-braille',
        'title' => 'Images',
        'page' => '/images'
    ],
    [
        'icon' => 'fa-sign-out',
        'title' => 'Sign out',
        'page' => '/logout'
    ]
];

function processMenu($menu)
{
    foreach ( $menu as $i => $item ) {
        // по умолчанию не нужно удалять элемент из списка меню
        $delete = false;
        // если в элементе ($item) меню есть массив условий по которым он должен отображаться
        if ( isset($item['when']) && is_array($item['when']) && is_array($item['when'][0]) ) {
            // перебираем весь массив условий по которым элемент должен показываться
            foreach ( $item['when'] as $function ) {
                // если в очередном условии есть элемент результата который должна вернуть функция условия
                if ( isset($function[2]) ) {
                    $state = $function[2];
                } else { // в очередном условии нет элемента результата, поэтому по-умолчанию будет true
                    $state = true;
                }
                unset($function[2]);
                // пытаемся вызвать функцию из условия
                // если она вернет не тот результат что указан в $state
                if ( call_user_func($function) !== $state ) {
                    // тогда удаляем этот элемент из списка меню
                    $delete = true;
                    break;
                }
            }
        } elseif ( isset($item['when']) && is_array($item['when']) && is_string($item['when'][0]) ) {
            // если в элементе ($item) меню есть одно условие по котором он должен отображаться

            // если в условии есть элемент результата который должна вернуть функция условия
            if ( isset($item['when'][2]) ) {
                $state = $item['when'][2];
            } else { // в условии нет элемента результата, поэтому по-умолчанию будет true
                $state = true;
            }
            unset($item['when'][2]);
            // пытаемся вызвать функцию из условия
            // если она вернет не тот результат что указан в $state
            if ( call_user_func($item['when']) !== $state ) {
                // тогда удаляем этот элемент из списка меню
                $delete = true;
            }
            // если указана динамическая функция
            // 'when' => function() {}
        } elseif ( isset($item['when']) && is_callable($item['when']) ) {
            if ( call_user_func($item['when']) !== false ) {
                $delete = true;
            }
        }

        if ( $delete ) {
            unset($menu[$i]);
        } elseif ( isset($item['submenu']) ) {
            $menu[$i]['submenu'] = processMenu($item['submenu']);
        }

        unset($menu[$i]['when']);
    }

    return $menu;
}

return processMenu($leftMenu);