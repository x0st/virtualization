<?php

return [
    'adminEmail' => 'admin@example.com',
    'pathForArchives' => '@app/uploaded-archives/'
];
