<?php
/**
 * Created by PhpStorm.
 * User: x0st
 * Date: 17.08.16
 * Time: 19:01
 */

namespace app\models;


use yii\db\ActiveRecord;

/**
 * Class ContainerExternalLink
 * @package app\models
 * @property mixed container_id
 * @property mixed name
 * @property mixed alias
 * @property mixed id
 */
class ContainerExternalLink extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return 'container_external_link';
    }
}