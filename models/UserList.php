<?php
/**
 * Список юзеров с пагинацией.
 * Created by PhpStorm.
 * User: x0st
 * Date: 12.08.16
 * Time: 23:42
 */

namespace app\models;


use avega\ListTable;
use yii\db\ActiveQuery;

class UserList
{
    /**
     * Список юзеров с пагинацией.
     * @return array
     */
    public static function getList()
    {
        return (new ListTable(User::className(), [
            'layout' => '{edit} {delete}',
            'linkPattern' => 'users/{action}/',
            'updateItem' => function($item) {
                // себя нельзя удалить
                if ($item['id'] == \Yii::$app->user->getId()) {
                    unset($item['actions']['delete']);
                }

                return $item;
            },
            'rules' => function (ActiveQuery $model) {
                return $model->select(['username', 'id', 'name']);
            }
        ]))->loadData();
    }
}