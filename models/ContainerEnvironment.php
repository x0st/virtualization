<?php
/**
 * Created by PhpStorm.
 * User: x0st
 * Date: 17.08.16
 * Time: 01:19
 */

namespace app\models;


use yii\db\ActiveRecord;

/**
 * Class ContainerEnvironment
 * @package app\models
 * @property mixed id
 * @property mixed name
 * @property mixed value
 * @property mixed container_id
 */
class ContainerEnvironment extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return 'container_environment';
    }
}