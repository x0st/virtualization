<?php
/**
 * Класс таблицы application_container.
 * Created by PhpStorm.
 * User: x0st
 * Date: 13.08.16
 * Time: 22:54
 */

namespace app\models;


use yii\db\ActiveRecord;

/**
 * Class ApplicationContainer
 * @package app\models
 * @property mixed id
 * @property mixed name
 * @property mixed application_id
 * @property mixed status
 * @property mixed image_type
 * @property mixed image_id
 * @property mixed image_branch
 * @property mixed restart
 */
class ApplicationContainer extends ActiveRecord
{
    /**
     * Для колонки в бд.
     * Когда следует перезагружать контейнер.
     */
    const RESTART_ALWAYS = 1;

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'application_container';
    }

    /**
     * Фк-ция для with().
     * @return \yii\db\ActiveQuery
     */
    public function getPorts()
    {
        return $this->hasMany(ContainerPort::className(), ['container_id' => 'id']);
    }

    /**
     * Фк-ция для with().
     * @return \yii\db\ActiveQuery
     */
    public function getLinks()
    {
        return $this->hasMany(ContainerLink::className(), ['container_id' => 'id']);
    }

    /**
     * Фк-ция для with().
     * @return \yii\db\ActiveQuery
     */
    public function getExternalLinks()
    {
        return $this->hasMany(ContainerExternalLink::className(), ['container_id' => 'id']);
    }

    /**
     * Фк-ция для with().
     * @return \yii\db\ActiveQuery
     */
    public function getSections()
    {
        return $this->hasMany(ContainerSection::className(), ['container_id' => 'id']);
    }


    /**
     * Фк-ция для with().
     * @return \yii\db\ActiveQuery
     */
    public function getDependencies()
    {
        return $this->hasMany(ContainerDependence::className(), ['container_id' => 'id']);
    }

    /**
     * Фк-ция для with().
     * @return \yii\db\ActiveQuery
     */
    public function getEnvironment()
    {
        return $this->hasMany(ContainerEnvironment::className(), ['container_id' => 'id']);
    }

    /**
     * Фк-ция для with().
     * @return \yii\db\ActiveQuery
     */
    public function getVolumesFrom()
    {
        return $this->hasMany(ContainerVolumeFrom::className(), ['container_id' => 'id']);
    }
}