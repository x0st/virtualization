<?php
/**
 * Класс таблицы container_section.
 * Created by PhpStorm.
 * User: x0st
 * Date: 15.08.16
 * Time: 19:33
 */

namespace app\models;


use yii\db\ActiveRecord;

/**
 * Class ContainerSection
 * @package app\models
 * @property mixed remote_path
 * @property mixed local_path
 * @property mixed path
 * @property mixed id
 */
class ContainerSection extends ActiveRecord
{
    /**
     * Имя таблицы.
     * @return string
     */
    public static function tableName()
    {
        return 'container_section';
    }
}