<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 23.08.16
 * Time: 12:37
 */

namespace app\models;


use yii\db\ActiveRecord;

class ContainerDependence extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return 'container_dependence';
    }
}