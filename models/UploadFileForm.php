<?php
/**
 * Created by PhpStorm.
 * User: x0st
 * Date: 15.08.16
 * Time: 23:40
 */

namespace app\models;


use yii\base\ErrorException;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class UploadFileForm
 * @package app\models
 * @property UploadedFile file
 */
class UploadFileForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $file;

    public $fileName;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['file', 'file', 'skipOnEmpty' => false, 'checkExtensionByMimeType' => false]
        ];
    }

    /**
     * @return bool
     * @throws ErrorException
     */
    public function save()
    {
        $fileName = time() . '_' . md5($this->file->name) . '.' . $this->file->extension;
        // проверка на существование директории для складывания файлов
        if (!isset(\Yii::$app->params['pathForArchives']) || !is_dir(\Yii::getAlias(\Yii::$app->params['pathForArchives']))) {
            throw new ErrorException('The directory for uploaded files does not exist or was not specified.');
        }

        $filePath = \Yii::getAlias(\Yii::$app->params['pathForArchives'] . $fileName);

        if ($this->file->saveAs($filePath)) {
            $this->fileName = $fileName;
            return true;
        }

        return false;
    }
}