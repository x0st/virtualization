<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\IdentityInterface;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginForm extends Model
{
    public $username;
    public $password;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required']
        ];
    }

    /**
     * @return bool
     */
    public function login()
    {
        if ($this->validate()) {

            $user = $this->getUser();

            if ($user !== null && $user->validatePassword($this->password)) {
                return Yii::$app->user->login($user);
            }
        }

        return false;
    }

    /**
     * @return null|User
     */
    public function getUser()
    {
        return User::findByUsername($this->username);
    }
}
