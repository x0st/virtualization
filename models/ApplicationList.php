<?php
/**
 * Массив приложений с пагинацией.
 * Created by PhpStorm.
 * User: x0st
 * Date: 13.08.16
 * Time: 23:10
 */

namespace app\models;


use avega\F;
use avega\ListTable;

/**
 * Class ApplicationList
 * @package app\models
 */
class ApplicationList
{
    /**
     * Возвращает массив приложений с пагинацией.
     * @return array
     */
    public static function getList()
    {
        ListTable::$defaultLinks['log'] = [
            'title' => 'Log',
            'icon' => 'fa-newspaper-o',
        ];

        $table = new ListTable(Application::className(), [
            'layout' => '{edit} {log} {delete}',
            'linkPattern' => 'applications/{action}/',
            'updateItem' => function ($item) {
                if ((bool)(int)$item['status']) {
                    $item['status'] = '<i class="fa fa-smile-o" aria-hidden="true" style="color: #26da14;font-weight: bolder;"></i>';
                } else {
                    $item['status'] = '<i class="fa fa-frown-o" aria-hidden="true" style="color: #dcdcdc;font-weight: bolder;"></i>';
                }
                return $item;
            }
        ]);

        return $table->loadData();
    }
}