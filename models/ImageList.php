<?php
/**
 * Список образов с пагинацией.
 * Created by PhpStorm.
 * User: x0st
 * Date: 13.08.16
 * Time: 02:42
 */

namespace app\models;


use avega\ListTable;

class ImageList
{
    /**
     * Возвращает список образов с пагинацией.
     * @return array
     */
    public static function getList()
    {
        return (new ListTable(Image::className(), [
            'layout' => '{edit} {delete}',
            'linkPattern' => 'images/{action}/'
        ]))->loadData();
    }
}