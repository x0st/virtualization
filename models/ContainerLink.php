<?php
/**
 * Класс таблицы container_link.
 * Created by PhpStorm.
 * User: x0st
 * Date: 15.08.16
 * Time: 16:34
 */

namespace app\models;


use yii\db\ActiveRecord;

/**
 * Class ContainerLink
 * @package app\models
 * @property mixed id
 * @property mixed linked_container_id
 * @property mixed linked_container_alias
 * @property mixed container_id
 */
class ContainerLink extends ActiveRecord
{
    /**
     * Имя таблицы.
     * @return string
     */
    public static function tableName()
    {
        return 'container_link';
    }
}