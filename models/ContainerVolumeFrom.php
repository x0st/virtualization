<?php
/**
 * Created by PhpStorm.
 * User: x0st
 * Date: 19.08.16
 * Time: 00:35
 */

namespace app\models;

use yii\db\ActiveRecord;

/**
 * Class ContainerVolumeFrom
 * @package app\models
 * @property mixed id
 * @property mixed container_id
 * @property mixed linked_container_id
 */
class ContainerVolumeFrom extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return 'container_volume_from';
    }
}