<?php
/**
 * Класс таблицы container_port.
 * Created by PhpStorm.
 * User: x0st
 * Date: 15.08.16
 * Time: 00:39
 */

namespace app\models;


use yii\db\ActiveRecord;

/**
 * Class ContainerPort
 * @package app\models
 * @property mixed local_port
 * @property mixed remote_port
 * @property mixed id
 */
class ContainerPort extends ActiveRecord
{
    /**
     * Имя таблицы.
     * @return string
     */
    public static function tableName()
    {
        return 'container_port';
    }
}