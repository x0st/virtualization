<?php
/**
 * Создание, обновление приложения. Управление контейнерами приложения.
 * Created by PhpStorm.
 * User: x0st
 * Date: 14.08.16
 * Time: 22:05
 */

namespace app\models;

use avega\EditableTable;
use avega\F;
use yii\base\DynamicModel;
use yii\validators\RangeValidator;

/**
 * Class ApplicationForm
 * @package app\models
 */
class ApplicationForm extends Application
{
    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';

    const IMAGE_TYPE = [
        'local' => 1,
        'remote' => 2,
        'dockerfile' => 3
    ];

    /**
     * Контейнеры из формы.
     * @var array
     */
    public $containers;

    /**
     * В случае обновления данных в контейнерах, здесь будут id новых строк таблиц контейнеров.
     * [
     *      {id контейнера} => [
     *          {таблица} => [
     *              {локальный id строки в таблице} => {id из бд|null}
     *          ]
     *      ]
     * ].
     * Локальный id строки в таблице => индекс строки в массиве всех строк таблицы на js стороне.
     * @var array
     */
    public $containersTablesIds;

    /**
     * В случае создания новых контейнеров, здесь будут id созданных контейнеров.
     * [
     *      {локальный id контейнера в форме} => {id из бд|null},
     *      ...
     * ].
     * Локальный id контейнера => индекс в массиве info на js стороне.
     * @var array
     */
    public $containersIds;

    /**
     * Таблицы из формы и их классы.
     * @var array
     */
    private $allowedTables = [
        'ports' => ContainerPort::class,
        'links' => ContainerLink::class,
        'sections' => ContainerSection::class,
        'environment' => ContainerEnvironment::class,
        'external_links' => ContainerExternalLink::class,
        'volumes_from' => ContainerVolumeFrom::class,
        'dependencies' => ContainerDependence::class
    ];

    /**
     * Сценарии формы.
     * @return array
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_CREATE => ['name', 'containers'],
            self::SCENARIO_UPDATE => ['name', 'containers']
        ];
    }

    /**
     * Правила валидации.
     * @return array
     */
    public function rules()
    {
        return [
            [['name', 'containers'], 'required'],
            ['containers', 'validateContainersArray']
        ];
    }

    /**
     * Проверяет массив с контейнерами на валидность.
     * @param $attribute
     */
    public function validateContainersArray($attribute)
    {
        $tablesRules = [
            'ports' => [
                [['local_port', 'remote_port'], 'required'],
                [['local_port', 'remote_port'], 'integer'],
            ],
            'external_links' => [
                [['name', 'alias'], 'required']
            ],
            'links' => [
                [['linked_container_id'], 'required'],
                ['linked_container_alias', 'default'],
                // проверка на существование контейнера
                ['linked_container_id', 'exist', 'targetAttribute' => 'id', 'targetClass' => 'app\models\ApplicationContainer', 'filter' => [
                    'application_id' => $this->id
                ]]
            ],
            'volumes_from' => [
                [['linked_container_id'], 'required'],
                // проверка на существование контейнера
                ['linked_container_id', 'exist', 'targetAttribute' => 'id', 'targetClass' => 'app\models\ApplicationContainer', 'filter' => [
                    'application_id' => $this->id
                ]]
            ],
            'sections' => [
                [['local_path', 'remote_path'], 'required'],
                ['file', 'default']
            ],
            'environment' => [
                [['name', 'value'], 'required']
            ],
            'dependencies' => [
                [['linked_container_id'], 'required'],
                // проверка на существование контейнера
                ['linked_container_id', 'exist', 'targetAttribute' => 'id', 'targetClass' => 'app\models\ApplicationContainer', 'filter' => [
                    'application_id' => $this->id
                ]]
            ]
        ];

        foreach ($this->containers as $container) {
            // данный массив имеет вид
            // [имя таблицы] => true|false,
            // [имя таблицы] => true|false,
            // ...
            // если у таблицы значение true
            // значит там есть ошибка валидации
            $validateTables = [];
            // прогоняем все таблицы и запускаем валидацию строк для каждой таблицы контейнера
            foreach ($this->allowedTables as $table => $tableFields) {
                $validateTables[$table] = isset($container[$table]) && !EditableTable::validate($tablesRules[$table], $container[$table]);
            }

            $validateContainer = DynamicModel::validateData($container, [
                [['_action', 'image_type', 'image_id', 'name', 'restart'], 'required'],
                ['_action', 'in', 'range' => ['create', 'delete', 'edit', 'none']],
                ['restart', 'in', 'range' => [ApplicationContainer::RESTART_ALWAYS]],
                // существование образа в бд
                ['image_id', 'exist', 'targetAttribute' => 'id', 'targetClass' => 'app\models\Image'],
                // существование ветки образа в бд
                ['image_branch', 'exist', 'targetAttribute' => 'id', 'targetClass' => 'app\models\ImageBranch'],
                ['image_type', 'in', 'range' => [self::IMAGE_TYPE['local'], self::IMAGE_TYPE['remote'], self::IMAGE_TYPE['dockerfile']]]
            ]);

            // проверка есть ли в массиве валидации таблиц значение true
            // если где-то есть true, значит есть ошибка валидации
            if ($validateContainer->hasErrors() || in_array(true, $validateTables, true)) {
                $this->addError($attribute, 'Containers have incorrect format.');
                break;
            }
        }
    }

    /**
     * Удаляет, обновляет, создает контейнеры для приложения.
     */
    private function updateContainers()
    {
        foreach ($this->containers as $i => $container) {
            $newId = null;

            switch ($container['_action']) {
                case 'delete':
                    $appContainer = ApplicationContainer::findOne($container['_id']);
                    if ($appContainer !== null) {
                        $appContainer->delete();
                    }
                    break;
                case 'create':
                    $appContainer = new ApplicationContainer();
                    $appContainer->name = $container['name'];
                    $appContainer->image_id = $container['image_id'];
                    $appContainer->image_type = $container['image_type'];
                    $appContainer->image_branch = $container['image_branch'];
                    $appContainer->application_id = $this->id;
                    $appContainer->restart = $container['restart'];

                    if ($appContainer->save(false)) {

                        $newId = $appContainer->id;

                        foreach ($this->allowedTables as $allowedTable => $tableClass) {
                            if (isset($container[$allowedTable])) {
                                $this->updateRowsInTable($appContainer->id, $allowedTable, $container[$allowedTable]);
                            }
                        }
                    }

                    break;
                case 'edit':
                    $appContainer = ApplicationContainer::findOne($container['_id']);

                    if ($appContainer !== null) {
                        $appContainer->image_id = $container['image_id'];
                        $appContainer->image_type = $container['image_type'];
                        $appContainer->image_branch = $container['image_branch'];
                        $appContainer->name = $container['name'];
                        $appContainer->restart = $container['restart'];

                        foreach ($this->allowedTables as $allowedTable => $tableClass) {
                            if (isset($container[$allowedTable])) {
                                $this->updateRowsInTable($appContainer->id, $allowedTable, $container[$allowedTable]);
                            }
                        }
                        $appContainer->save(false);
                    }

                    break;
            }

            $this->containersIds[$i] = $newId;
        }
    }

    /**
     * @param $containerId integer
     * @param $tableName string
     * @param $rows array
     */
    private function updateRowsInTable($containerId, $tableName, $rows) {
        $table = new EditableTable($this->allowedTables[$tableName]);
        $table->setValues($rows);
        $this->containersTablesIds[$containerId][$tableName] = $table->save(['container_id' => $containerId]);
    }

    /**
     * После сохранения приложения обновляем его контейнеры.
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        $this->updateContainers();
        parent::afterSave($insert, $changedAttributes);
    }
}