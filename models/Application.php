<?php
/**
 * Класс таблицы application.
 * Created by PhpStorm.
 * User: x0st
 * Date: 13.08.16
 * Time: 22:47
 */

namespace app\models;


use yii\db\ActiveRecord;

/**
 * Class Application
 * @package app\models
 * @property mixed name
 * @property mixed id
 * @property mixed containers
 * @property mixed restart
 * @property mixed status
 * @property mixed log
 */
class Application extends ActiveRecord
{
    /**
     * Имя таблицы.
     * @return string
     */
    public static function tableName()
    {
        return 'application';
    }

    /**
     * Фк-ция для with().
     * @return \yii\db\ActiveQuery
     */
    public function getContainers()
    {
        return $this->hasMany(ApplicationContainer::className(), ['application_id' => 'id']);
    }
}