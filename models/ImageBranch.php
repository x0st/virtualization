<?php
/**
 * Класс таблицы image_branch.
 * Created by PhpStorm.
 * User: x0st
 * Date: 13.08.16
 * Time: 15:11
 */

namespace app\models;


use yii\db\ActiveRecord;

/**
 * Class ImageBranch
 * @package app\models
 * @property mixed image_id
 * @property mixed name
 * @property mixed build
 * @property mixed id
 */
class ImageBranch extends ActiveRecord
{
    /**
     * Имя таблицы.
     * @return string
     */
    public static function tableName()
    {
        return 'image_branch';
    }
}