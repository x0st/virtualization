<?php
/**
 * Created by PhpStorm.
 * User: x0st
 * Date: 22.08.16
 * Time: 02:35
 */

namespace app\models;
use avega\F;


/**
 * Клонирует приложение со всеми его контейнерами.
 * Class ApplicationClone
 * @package app\models
 */
class ApplicationClone
{
    /**
     * @var Application
     */
    private $application;

    /**
     * @var ApplicationContainer[]
     */
    private $containers = [];

    /**
     * @var ApplicationContainer[]
     */
    private $newContainers = [];

    /**
     * ApplicationClone constructor.
     * @param $id int id приложения, которое нужно клонировать.
     */
    public function __construct($id)
    {
        $this->application = Application::findOne($id);
        $this->containers = ApplicationContainer::findAll(['application_id' => $id]);
    }

    /**
     * @return null|int
     */
    public function cloneApp()
    {
        $transaction = Application::getDb()
            ->beginTransaction();

        if (($appId = $this->createApp()) && $this->createContainers($appId) && $this->cloneTables()) {
            $transaction->commit();
            return $appId;
        }

        $transaction->rollBack();

        return null;
    }

    /**
     * Клонирует таблицы контейнеров.
     * @return bool
     */
    private function cloneTables()
    {
        foreach ($this->containers as $i => $container) {
            if (!$this->cloneContainerEnvironmentTable($container->id, $this->newContainers[$i]->id) ||
                !$this->cloneContainerExternalLinkTable($container->id, $this->newContainers[$i]->id) ||
                !$this->cloneContainerPortTable($container->id, $this->newContainers[$i]->id) ||
                !$this->cloneContainerSectionTable($container->id, $this->newContainers[$i]->id)
            ) {
                return false;
            }
        }

        return true;
    }

    /**
     * Берет строки из таблицы для контейнера $containerId и вставляет их для контейнера $newContainerId.
     * @param $containerId
     * @param $newContainerId
     * @return bool
     */
    private function cloneContainerPortTable($containerId, $newContainerId)
    {
        $rows = ContainerPort::find()->where(['container_id' => $containerId])->asArray()->all();

        if (count($rows) === 0) {
            return true;
        }

        $rows = array_map(function ($row) use ($newContainerId) {
            return [
                'container_id' => $newContainerId,
                'local_port' => $row['local_port'],
                'remote_port' => $row['remote_port']
            ];
        }, $rows);

        return ContainerPort::getDb()
            ->createCommand()
            ->batchInsert(ContainerPort::tableName(), array_keys($rows[0]), $rows)
            ->execute() == count($rows);
    }

    /**
     * Берет строки из таблицы для контейнера $containerId и вставляет их для контейнера $newContainerId.
     * @param $containerId
     * @param $newContainerId
     * @return bool
     */
    private function cloneContainerEnvironmentTable($containerId, $newContainerId)
    {
        $rows = ContainerEnvironment::find()->where(['container_id' => $containerId])->asArray()->all();

        if (count($rows) === 0) {
            return true;
        }

        $rows = array_map(function ($row) use ($newContainerId) {
            return [
                'container_id' => $newContainerId,
                'name' => $row['name'],
                'value' => $row['value']
            ];
        }, $rows);

        return ContainerEnvironment::getDb()
            ->createCommand()
            ->batchInsert(ContainerEnvironment::tableName(), array_keys($rows[0]), $rows)
            ->execute() == count($rows);
    }


    /**
     * Берет строки из таблицы для контейнера $containerId и вставляет их для контейнера $newContainerId.
     * @param $containerId
     * @param $newContainerId
     * @return bool
     */
    private function cloneContainerExternalLinkTable($containerId, $newContainerId)
    {
        $rows = ContainerExternalLink::find()->where(['container_id' => $containerId])->asArray()->all();

        if (count($rows) === 0) {
            return true;
        }

        $rows = array_map(function ($row) use ($newContainerId) {
            return [
                'container_id' => $newContainerId,
                'name' => $row['name'],
                'alias' => $row['alias']
            ];
        }, $rows);

        return ContainerExternalLink::getDb()
            ->createCommand()
            ->batchInsert(ContainerExternalLink::tableName(), array_keys($rows[0]), $rows)
            ->execute() == count($rows);
    }


    /**
     * Берет строки из таблицы для контейнера $containerId и вставляет их для контейнера $newContainerId.
     * @param $containerId
     * @param $newContainerId
     * @return bool
     */
    private function cloneContainerSectionTable($containerId, $newContainerId)
    {
        $rows = ContainerSection::find()->where(['container_id' => $containerId])->asArray()->all();

        if (count($rows) === 0) {
            return true;
        }

        $rows = array_map(function ($row) use ($newContainerId) {
            return [
                'container_id' => $newContainerId,
                'remote_path' => $row['remote_path'],
                'local_path' => $row['local_path'],
                'file' => $row['file']
            ];
        }, $rows);

        return ContainerSection::getDb()
            ->createCommand()
            ->batchInsert(ContainerSection::tableName(), array_keys($rows[0]), $rows)
            ->execute() == count($rows);
    }


    /**
     * @param $appId
     * @return bool
     */
    private function createContainers($appId)
    {
        foreach ($this->containers as $container) {
            $newContainer = new ApplicationContainer();
            $newContainer->application_id = $appId;
            $newContainer->name = $container->name;
            $newContainer->image_id = $container->image_id;
            $newContainer->image_type = $container->image_type;
            $newContainer->image_branch = $container->image_branch;

            if (!$newContainer->save(false)) {
                return false;
            }

            $this->newContainers[] = $newContainer;
        }

        return true;
    }

    /**
     * Создает приложение с суфиксом '_clone' и возвращает его id.
     * @return integer|null
     */
    private function createApp()
    {
        $app = new Application();
        $app->name = $this->application->name . '_clone';

        return $app->save(false) ? $app->id : null;
    }
}