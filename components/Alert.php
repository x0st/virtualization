<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 24/04/2016
 * Time: 19:32
 */

namespace app\components;

class Alert
{
    /**
     * @param string $type
     * @return string
     */
    public static function alert($type = 'success')
    {
        if ( is_array($type) ) {
            foreach ( $type as $alert ) {
                echo self::html($alert);
            }
        } else {
            echo self::html($type);
        }
    }

    /**
     * @param $type
     * @return string
     */
    private static function html($type)
    {
        return <<<HTML
        <div class="alert alert-{$type} {$type}_container" style="margin: 15px;display: none;">
            <strong><span class="{$type}_message"></span></strong>
        </div> 
HTML;

    }
}