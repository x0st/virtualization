<?php

use yii\db\Migration;

/**
 * Handles adding image_branch to table `application_container`.
 */
class m160823_105146_add_image_branch_column_to_application_container_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('application_container', 'image_branch', $this->bigInteger()->notNull()->unsigned()->comment(''));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('application_container', 'image_branch');
    }
}
