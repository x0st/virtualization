<?php

use yii\db\Migration;

/**
 * Handles adding restart to table `application_container`.
 */
class m160818_212026_add_restart_column_to_application_container_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('application_container', 'restart', $this->smallInteger(1)->notNull()->unsigned()->defaultValue(1)->comment('Когда нужно перезагружать контейнер: 1 - всегда'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('application_container', 'restart');
    }
}
