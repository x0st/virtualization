<?php

use yii\db\Migration;

/**
 * Handles the creation for table `container_external_link`.
 */
class m160817_143343_create_container_external_link_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('container_external_link', [
            'id' => $this->primaryKey(),
            'name' => $this->string(200)->notNull()->comment(''),
            'alias' => $this->string(200)->comment(''),
            'container_id' => $this->bigInteger()->notNull()->unsigned()->comment('')
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('container_external_link');
    }
}
