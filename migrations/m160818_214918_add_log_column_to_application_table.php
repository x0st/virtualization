<?php

use yii\db\Migration;

/**
 * Handles adding log to table `application`.
 */
class m160818_214918_add_log_column_to_application_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('application', 'log', $this->text()->null()->comment('Лог приложения'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('application', 'log');
    }
}
