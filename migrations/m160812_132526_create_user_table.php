<?php

use yii\db\Migration;

/**
 * Handles the creation for table `user`.
 */
class m160812_132526_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'username' => $this->string(200)->notNull()->unique()->comment(''),
            'password_hash' => $this->string(200)->notNull()->comment(''),
            'name' => $this->string(200)->notNull()->comment(''),
            'auth_key' => $this->string(100)->comment('')
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user');
    }
}
