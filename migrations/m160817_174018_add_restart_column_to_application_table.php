<?php

use yii\db\Migration;

/**
 * Handles adding restart to table `application`.
 */
class m160817_174018_add_restart_column_to_application_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('application', 'restart', $this->boolean()->defaultValue(0)->comment('Нужно ли перезапускать приложение или нет'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('application', 'restart');
    }
}
