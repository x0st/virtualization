<?php

use yii\db\Migration;

/**
 * Handles the creation for table `image_branch`.
 */
class m160812_132421_create_image_branch_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('image_branch', [
            'id' => $this->primaryKey(),
            'image_id' => $this->bigInteger()->notNull()->unsigned()->comment(''),
            'name' => $this->string(200)->notNull()->unsigned()->comment(''),
            'build' => $this->boolean()->defaultValue(0)->comment('')
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('image_branch');
    }
}
