<?php

use yii\db\Migration;

/**
 * Handles adding name to table `application_container`.
 */
class m160813_195226_add_name_column_to_application_container_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('application_container', 'name', $this->string(200)->notNull()->comment(''));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
    }
}
