<?php

use yii\db\Migration;

/**
 * Handles the creation for table `application`.
 */
class m160812_132438_create_application_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('application', [
            'id' => $this->primaryKey(),
            'name' => $this->string(200)->notNull()->comment('')
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('application');
    }
}
