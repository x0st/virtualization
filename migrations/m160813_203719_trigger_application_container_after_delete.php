<?php

use yii\db\Migration;

class m160813_203719_trigger_application_container_after_delete extends Migration
{
    public function up()
    {
        $sql = file_get_contents(__DIR__ . '/TriggerDeleteApplicationContainer.sql');
        $this->execute($sql);
    }

    public function down()
    {
        $this->execute('DROP TRIGGER IF EXISTS DeleteApplicationContainer;');
    }
}
