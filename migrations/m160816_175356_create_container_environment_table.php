<?php

use yii\db\Migration;

/**
 * Handles the creation for table `container_environment`.
 */
class m160816_175356_create_container_environment_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('container_environment', [
            'id' => $this->primaryKey(),
            'name' => $this->string(200)->notNull()->comment('Имя переменной'),
            'value' => $this->string(200)->notNull()->comment('Значения переменной'),
            'container_id' => $this->bigInteger()->notNull()->unsigned()->comment('К какому контейнеру относиться')
        ]);

        $this->addCommentOnTable('container_environment', 'Доступные переменные для контейнера');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('container_environment');
    }
}
