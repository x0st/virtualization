<?php

use yii\db\Migration;

/**
 * Handles the creation for table `application_container`.
 */
class m160812_132450_create_application_container_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('application_container', [
            'id' => $this->primaryKey(),
            'application_id' => $this->bigInteger()->unsigned()->notNull()->comment('')
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('application_container');
    }
}
