<?php

use yii\db\Migration;

/**
 * Handles the creation for table `container_port`.
 */
class m160812_132504_create_container_port_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('container_port', [
            'id' => $this->primaryKey(),
            'container_id' => $this->bigInteger()->notNull()->unsigned()->comment(''),
            'local_port' => $this->string(50)->comment(''),
            'remote_port' => $this->string(50)->comment('')
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('container_port');
    }
}
