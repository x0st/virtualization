<?php

use yii\db\Migration;

/**
 * Handles adding status to table `application`.
 */
class m160817_174005_add_status_column_to_application_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('application', 'status', $this->boolean()->defaultValue(0)->comment('1 - запущен, 0 - мертв'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('application', 'status');
    }
}
