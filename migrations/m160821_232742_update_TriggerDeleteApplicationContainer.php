<?php

use yii\db\Migration;

class m160821_232742_update_TriggerDeleteApplicationContainer extends Migration
{
    public function up()
    {
        $sql = file_get_contents(__DIR__ . '/22_08_2016_02_28_TriggerDeleteApplicationContainer.sql');
        $this->execute($sql);
    }

    public function down()
    {
        $this->execute('DROP TRIGGER IF EXISTS DeleteApplicationContainer;');
    }
}
