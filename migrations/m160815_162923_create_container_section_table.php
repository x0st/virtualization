<?php

use yii\db\Migration;

/**
 * Handles the creation for table `container_section`.
 */
class m160815_162923_create_container_section_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('container_section', [
            'id' => $this->primaryKey(),
            'container_id' => $this->bigInteger()->notNull()->unsigned()->comment(''),
            'remote_path' => $this->string(200)->comment(''),
            'local_path' => $this->string(200)->comment(''),
            'file' => $this->string(200)->comment('')
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('container_section');
    }
}
