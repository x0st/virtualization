<?php

use yii\db\Migration;

/**
 * Handles adding image_id to table `application_container`.
 */
class m160817_185918_add_image_id_column_to_application_container_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('application_container', 'image_id', $this->bigInteger()->notNull()->unsigned()->comment('ID образа из таблицы образов'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
         $this->dropColumn('application_container', 'image_id');
    }
}
