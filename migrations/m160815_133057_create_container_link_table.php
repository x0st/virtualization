<?php

use yii\db\Migration;

/**
 * Handles the creation for table `container_link`.
 */
class m160815_133057_create_container_link_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('container_link', [
            'id' => $this->primaryKey(),
            'container_id' => $this->bigInteger()->notNull()->unsigned()->comment(''),
            'linked_container_id' => $this->bigInteger()->notNull()->unsigned()->comment(''),
            'linked_container_alias' => $this->string(200)->comment('')
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('container_link');
    }
}
