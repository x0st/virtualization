<?php

use yii\db\Migration;

/**
 * Handles adding image_type to table `application_container`.
 */
class m160817_185942_add_image_type_column_to_application_container_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('application_container', 'image_type', $this->smallInteger(1)->notNull()->unsigned()->comment('Тип образа: 1 - local, 2 - remote, 3 - dockerfile'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('application_container', 'image_type');
    }
}
