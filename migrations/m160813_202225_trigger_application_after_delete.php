<?php

use yii\db\Migration;

class m160813_202225_trigger_application_after_delete extends Migration
{
    public function up()
    {
        $sql = file_get_contents(__DIR__ . '/TriggerDeleteApplication.sql');
        $this->execute($sql);
    }

    public function down()
    {
        $this->execute('DROP TRIGGER IF EXISTS DeleteApplication;');
    }
}
