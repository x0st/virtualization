<?php

use yii\db\Migration;

/**
 * Handles adding status to table `application_container`.
 */
class m160817_181744_add_status_column_to_application_container_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('application_container', 'status', $this->boolean()->defaultValue(0)->comment('Работает ли контейнер или нет'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('application_container', 'status');
    }
}
