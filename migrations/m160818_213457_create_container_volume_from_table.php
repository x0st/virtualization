<?php

use yii\db\Migration;

/**
 * Handles the creation for table `container_volume_from`.
 */
class m160818_213457_create_container_volume_from_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('container_volume_from', [
            'id' => $this->primaryKey(),
            'container_id' => $this->bigInteger()->notNull()->unsigned()->comment(''),
            'linked_container_id' => $this->bigInteger()->notNull()->unsigned()->comment(''),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('container_volume_from');
    }
}
