DROP TRIGGER IF EXISTS DeleteApplicationContainer;

CREATE TRIGGER DeleteApplicationContainer AFTER DELETE ON application_container FOR EACH ROW
  BEGIN
    DELETE FROM container_port
    WHERE container_port.container_id = old.id;

    DELETE FROM container_link
    WHERE container_link.container_id = old.id;

    DELETE FROM container_section
    WHERE container_section.container_id = old.id;

    DELETE FROM container_volume_from
    WHERE container_volume_from.container_id = old.id;

    DELETE FROM container_environment
    WHERE container_environment.container_id = old.id;

    DELETE FROM container_external_link
    WHERE container_external_link.container_id = old.id;
  END;

