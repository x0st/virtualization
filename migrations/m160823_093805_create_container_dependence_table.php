<?php

use yii\db\Migration;

/**
 * Handles the creation for table `container_dependence`.
 */
class m160823_093805_create_container_dependence_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('container_dependence', [
            'id' => $this->primaryKey(),
            'linked_container_id' => $this->bigInteger()->notNull()->unsigned()->comment(''),
            'container_id' => $this->bigInteger()->notNull()->unsigned()->comment('')
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('container_dependence');
    }
}
