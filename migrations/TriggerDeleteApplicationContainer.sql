CREATE TRIGGER DeleteApplicationContainer AFTER DELETE ON application_container FOR EACH ROW
  BEGIN
    DELETE FROM container_port
    WHERE container_port.container_id = old.id;
  END;