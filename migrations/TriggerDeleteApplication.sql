CREATE TRIGGER DeleteApplication AFTER DELETE ON application FOR EACH ROW
  BEGIN
    DELETE FROM application_container
    WHERE application_container.application_id = old.id;
  END;