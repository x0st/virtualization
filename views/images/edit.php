<?php
/**
 * Created by PhpStorm.
 * User: x0st
 * Date: 13.08.16
 * Time: 17:02
 * @var $image \app\models\Image
 * @var $branches \app\models\ImageBranch[]
 */
?>
<div class="content-header">
    <div class="row">
        <div class="col-md-6 pull-left">
            <h3>Image: <?= $image->name ?></h3>
        </div>
    </div>
</div>
<div data-alert-form></div>
<div class="content">
    <div class="box box-solid">
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name">Name</label>
                         <input type="text" id="name" class="form-control" value="<?= $image->name ?>">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-warning btn-lg btn-update">Update</button>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group"></div>
                    <table id="branches" class="table-bordered table-striped table">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Build</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {

        var table = new EditableTable('branches', {
            'tableSelector': '#branches',
            'structure': [
                {'name': 'name', 'type': 'string'},
                {'name': 'build', 'type': 'checkbox'}
            ]
        });

        // в таблицу добавляем ветки образа
        <?php if ($branches !== null): ?>
        <?php foreach ($branches as $branch): ?>
            table.addRow({
                _id: <?= $branch->id ?>,
                name: '<?= $branch->name ?>',
                build: '<?= $branch->build ?>'
            }, false);
        <?php endforeach ?>
        <?php endif ?>

        table.displayRows();

        // перед отправкой формы собираем ветки
        Form.prototype.beforeSubmit = function () {
            return {'branches': table.compileRows()};
        };

        // после отправки формы очищаем таблицу от удаленных строк, подставляем новые id, если строки были созданы,
        // отображаем заново
        Form.prototype.afterUpdate = function (response) {

            if (response.status == 1) {
                table.updateIds(response.updates);
            }

            table.cleanDeletedRows();
            table.displayRows();
            this.afterSubmit(response, 'update');
        };

        new Form('image', {
            'action': 'update',
            'url': '<?= \yii\helpers\Url::to(['/images/save/' . $image->id]) ?>',
            'fields': ['name']
        });
    });
</script>
