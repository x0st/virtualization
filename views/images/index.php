<?php
/**
 * Created by PhpStorm.
 * User: x0st
 * Date: 13.08.16
 * Time: 02:50
 */
?>
<div class="content-header">
    <div class="row">
        <div class="col-md-6 pull-left">
            <h3>Images</h3>
        </div>
        <div class="col-md-6 pull-right">
            <a href="<?= \yii\helpers\Url::to(['/images/new']) ?>" class="btn btn-warning pull-right">New image</a>
        </div>
    </div>
</div>
<div data-alert-list-table></div>
<div class="content">
    <div class="box box-solid">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                <?= \app\widgets\ListTable::widget([
                        'id' => 'images',
                        'columns' => [
                            'id' => ['Id'],
                            'name' => ['Name'],
                            'actions' => true,
                        ],
                        'pagination' => true
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        (new ListTable('images', {
            structure: ['id', 'name'],
            tableSelector: '#images',
            url: '<?= \yii\helpers\Url::to(['/images/get-list']) ?>',
            actions: {
                'edit': {ajax: false},
                'delete': {ajax: true}
            }
        })).load();
    });
</script>