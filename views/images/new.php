<?php
/**
 * Created by PhpStorm.
 * User: x0st
 * Date: 13.08.16
 * Time: 03:44
 */
?>
<div class="content-header">
    <div class="row">
        <div class="col-md-6 pull-left">
            <h3>New image</h3>
        </div>
    </div>
</div>
<div data-alert-form></div>
<div class="content">
    <div class="box box-solid">
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" id="name" class="form-control">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-warning btn-lg btn-create">Create</button>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group"></div>
                    <table id="branches" class="table-bordered table-striped table">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Build</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {

        var table = new EditableTable('branches', {
            'tableSelector': '#branches',
            'structure': [
                {'name': 'name', 'type': 'string'},
                {'name': 'build', 'type': 'checkbox'}
            ]
        });

        table.displayRows();

        // перед отправкой формы собираем ветки
        Form.prototype.beforeSubmit = function () {
            return {'branches': table.compileRows()};
        };

        // после отправки формы очищаем таблицу от удаленных строк, отображаем заново
        Form.prototype.afterCreate = function (response) {
            table.cleanDeletedRows();
            table.displayRows();
            this.afterSubmit(response, 'create');
        };

        new Form('image', {
            'action': 'create',
            'url': '<?= \yii\helpers\Url::to(['/images/save']) ?>',
            'fields': ['name']
        });
    });
</script>