<?php

/* @var $this yii\web\View */
/* @var $model app\models\LoginForm */

$loginError = Yii::$app->session->getFlash('LoginError', false);
$this->title = 'Login';
?>
<div class="login-box">
    <div class="login-logo">
        <a href="#"><b><?= Yii::$app->name ?></b></a>
    </div>
    <div class="login-box-body row">
        <?php if ( $loginError ): ?>
            <p class="login-box-msg" style="color:red;">
                <?= $loginError ?>
            </p>
        <?php else: ?>
            <p class="login-box-msg">
                <?= Yii::t('app', 'Sign in to start your session') ?>
            </p>
        <?php endif ?>
        <form action="" method="POST">
            <div class="form-group has-feedback">
                <input tabindex="1" type="text" name="form[username]" class="form-control" placeholder="Username" autofocus>
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input tabindex="2" type="password" name="form[password]" class="form-control" placeholder="Password">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <input type="hidden" name="_csrf" value="<?= Yii::$app->request->csrfToken ?>">
                    <button type="submit" tabindex="3" class="btn btn-primary btn-block btn-flat btn-login">Login</button>
                </div>
            </div>
        </form>
    </div>
</div>