<?php
/**
 * Created by PhpStorm.
 * User: x0st
 * Date: 12.08.16
 * Time: 23:01
 * @var $content
 */
use yii\helpers\Html;
use yii\helpers\Url;

\app\assets\JqueryAsset::register($this);
\app\assets\BootstrapAsset::register($this);
\app\assets\AdminLTEAsset::register($this);
\app\assets\FontAwesomeAsset::register($this);
\app\assets\ListTableAsset::register($this);
\app\assets\EditableTableAsset::register($this);
\app\assets\FormAsset::register($this);
\app\assets\BsAlertAsset::register($this);
\app\assets\AppAsset::register($this);

$leftMenu = require(__DIR__ . '/../../config/left-menu.php');
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <title><?= $this->title ?></title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?= Html::csrfMetaTags() ?>
    <?php $this->head() ?>
    <script>
        window._csrf = $('meta[name=csrf-token]').attr('content');
    </script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<?php $this->beginBody() ?>
<div class="wrapper">
    <header class="main-header">
        <a class="logo">
            <span class="logo-mini"><?= Yii::$app->name ?></span>
            <span class="logo-lg"><?= Yii::$app->name ?></span>
        </a>
        <nav class="navbar navbar-static-top" role="navigation">
            <a onmouseenter="$(this).css('text-decoration', 'none');" class="sidebar-toggle" data-toggle="offcanvas"
               role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-user"></i>
                            <span class="hidden-xs"><?= Yii::$app->user->identity->name ?></span>
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <aside class="main-sidebar">
        <section class="sidebar">
            <ul class="sidebar-menu">
                <?php foreach ($leftMenu as $index => $item): ?>
                    <li class="treeview" tabindex="<?= $index ?>">
                        <a href="<?= Url::to([$item['page']]) ?>">
                            <i class="fa <?= $item['icon'] ?>"></i>
                            <span><?= $item['title'] ?></span>
                            <?php if (isset($item['submenu'])): ?>
                                <i class="fa fa-angle-left pull-right"></i>
                            <?php endif; ?>
                        </a>
                        <?php if (isset($item['submenu'])): ?>
                            <ul class="treeview-menu">
                                <?php foreach ($item['submenu'] as $subitem): ?>

                                    <li>
                                        <a href="<?= Url::to($subitem['page']) ?>">
                                            <i class="fa <?= $subitem['icon'] ?>"></i>
                                            <?= $subitem['title'] ?>
                                        </a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                    </li>
                <?php endforeach; ?>
            </ul>
        </section>
    </aside>
    <div class="content-wrapper">
        <?= $content ?>
    </div>
    <footer class="main-footer">
        <input type="hidden" name="_csrf" value="<?= Yii::$app->request->csrfToken ?>">
        <div class="pull-right hidden-xs"><b>Version</b> <?= Yii::$app->version ?></div>
        <strong>Copyright © <?= date('Y') ?> <?= Yii::$app->name ?>.</strong>
        All rights reserved.
    </footer>
    <div class="control-sidebar-bg"></div>
</div>
<div class="modal-ajax"></div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
