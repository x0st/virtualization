<?php
/**
 * Created by PhpStorm.
 * User: x0st
 * Date: 13.08.16
 * Time: 03:30
 * @var $user \app\models\User
 */
?>
<div class="content-header">
    <div class="row">
        <div class="col-md-6 pull-left">
            <h3>User: <?= $user->name ?></h3>
        </div>
    </div>
</div>
<div data-alert-form></div>
<div class="content">
    <div class="box box-solid">
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" id="name" class="form-control" value="<?= $user->name ?>">
                    </div>
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" id="username" class="form-control" value="<?= $user->username ?>">
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="text" id="password" class="form-control" value="">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-warning btn-lg btn-update">Update</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        // после сохранения обновляем имя юзера в хедере страницы
        Form.prototype.afterUpdate = function (response, data) {
            $('.content-header').find('h3').text('User: ' + data['name']);
            this.afterSubmit(response, 'update');
        };

        new Form('user', {
            'action': 'update',
            'url': '<?= \yii\helpers\Url::to(['/users/save/' . $user->id]) ?>',
            'fields': ['name', 'username', 'password']
        });
    });
</script>
