<?php
/**
 * Created by PhpStorm.
 * User: x0st
 * Date: 13.08.16
 * Time: 00:10
 */
?>
<div class="content-header">
    <div class="row">
        <div class="col-md-6 pull-left">
            <h3>New user</h3>
        </div>
    </div>
</div>
<div data-alert-form></div>
<div class="content">
    <div class="box box-solid">
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" id="name" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" id="username" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="text" id="password" class="form-control">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-warning btn-lg btn-create">Create</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        new Form('user', {
            'action': 'create',
            'url': '<?= \yii\helpers\Url::to(['/users/save']) ?>',
            'fields': ['name', 'username', 'password']
        });
    });
</script>