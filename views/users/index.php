<?php
/**
 * Created by PhpStorm.
 * User: x0st
 * Date: 12.08.16
 * Time: 23:09
 */
?>
<div class="content-header">
    <div class="row">
        <div class="col-md-6 pull-left">
            <h3>Users</h3>
        </div>
        <div class="col-md-6 pull-right">
            <a href="<?= \yii\helpers\Url::to(['/users/new']) ?>" class="btn btn-warning pull-right">New user</a>
        </div>
    </div>
</div>
<div data-alert-list-table></div>
<div class="content">
    <div class="box-solid box">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <?= \app\widgets\ListTable::widget([
                        'id' => 'users',
                        'columns' => [
                            'id' => ['Id'],
                            'name' => ['Name'],
                            'username' => ['Username'],
                            'actions' => true,
                        ],
                        'pagination' => true
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        (new ListTable('users', {
            structure: ['id', 'name', 'username'],
            tableSelector: '#users',
            url: '<?= \yii\helpers\Url::to(['/users/get-list']) ?>',
            actions: {
                'edit': {ajax: false},
                'delete': {ajax: true}
            }
        })).load();
    });
</script>