<?php
/**
 * Created by PhpStorm.
 * User: x0st
 * Date: 19.08.16
 * Time: 00:47
 * @var $application \app\models\Application
 */
?>
<div class="content-header">
    <div class="row">
        <div class="col-md-6 pull-left">
            <h3><?= $application->name ?>'s log</h3>
        </div>
    </div>
</div>
<div class="content">
    <div class="box box-solid">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <pre><?= empty($application->log) ? 'There is no log yet' : $application->loggit ?></pre>
                </div>
            </div>
        </div>
    </div>
</div>
