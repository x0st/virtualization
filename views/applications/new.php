<?php
/**
 * Created by PhpStorm.
 * User: x0st
 * Date: 14.08.16
 * Time: 00:01
 * @var $this \yii\web\View
 * @var $images \app\models\Image[]
 */
$this->registerCssFile(\yii\helpers\Url::to(['/css/applications.manage.css']));
?>
<div class="content-header">
    <div class="row">
        <div class="col-md-6 pull-left">
            <h3>New application</h3>
        </div>
    </div>
</div>
<div data-alert-form></div>
<div class="content">
    <div class="box box-solid">
        <div class="box-body">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" id="name" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Containers</label>
                        <div class="containers">
                        </div>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-lg btn-warning btn-create">Create</button>
                        <button class="btn btn-lg btn-info add-container">Add a container</button>
                    </div>
                </div>
                <div class="col-md-9 options">
                    <div class="form-group">
                        <label for="image-id">Image</label>
                        <select id="image-id" class="form-control">
                            <?php foreach ($images as $image): ?>
                                <option value="<?= $image->id ?>"><?= $image->name ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="image-branch">Image branch</label>
                        <select id="image-branch" class="form-control"></select>
                    </div>
                    <div class="form-group">
                        <label>Image type</label>
                        <div class="radio">
                            <label>
                                <input type="radio" name="image-type"
                                       value="<?= \app\models\ApplicationForm::IMAGE_TYPE['local'] ?>">
                                Local image
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="image-type"
                                       value="<?= \app\models\ApplicationForm::IMAGE_TYPE['remote'] ?>">
                                Remote image
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="image-type"
                                       value="<?= \app\models\ApplicationForm::IMAGE_TYPE['dockerfile'] ?>">
                                Dockerfile
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="restart">Restart</label>
                        <select id="restart" class="form-control">
                            <option value="<?= \app\models\ApplicationContainer::RESTART_ALWAYS ?>">Always</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="ports">Ports</label>
                        <table id="ports" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Local port</th>
                                <th>Remote port</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                    <div class="form-group">
                        <label for="sections">Sections</label>
                        <table id="sections" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Local path</th>
                                <th>Remote path</th>
                                <th>File</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                    <div class="form-group">
                        <label for="external-links">External links</label>
                        <table id="external-links" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Alias</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                    <div class="form-group">
                        <label for="environment">Environment</label>
                        <table id="environment" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Value</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var branches = {};

    // массив веток для каждого образа
    <?php foreach ($images as $image): ?>
    branches[<?= $image->id ?>] = <?= json_encode(array_map(function ($branch) {
        return ['id' => $branch->id, 'name' => $branch->name];
    }, $image->branches)) ?>;
    <?php endforeach; ?>

    /**
     * Устанавливает ветки в select для образа по его id.
     * @param imageId {int}
     */
    function setBranchesForImage(imageId) {
        $('#image-branch').find('option').remove();

        if (branches[imageId]) {
            $.each(branches[imageId], function (index, branch) {
                $('#image-branch').append('<option value="' + branch.id + '">' + branch.name + '</option>');
            });
        }
    }

    // после изменения образа
    // устанавливаем ветки для него
    $('#image-id').change(function () {
        setBranchesForImage($(this).val());
    });

    var body = $('body');
    var tables = {};
    // таблица управления портами
    tables['ports'] = new EditableTable('ports', {
        'tableSelector': '#ports',
        'structure': [
            {'name': 'local_port', 'type': 'string'},
            {'name': 'remote_port', 'type': 'string'}
        ]
    });
    // таблица управление разделами
    tables['sections'] = new EditableTable('sections', {
        'tableSelector': '#sections',
        'structure': [
            {'name': 'local_path', 'type': 'string'},
            {'name': 'remote_path', 'type': 'string'},
            {
                'name': 'file',
                'type': 'file',
                // сюда будет загружаться файл
                'url': '<?= \yii\helpers\Url::to(['/applications/upload-archive']) ?>',
                // сообщение об ошибке, если вдруг запрос не дойдет до сервера
                'error-message': 'Error of file uploading'
            }
        ]
    });
    tables['external_links'] = new EditableTable('external_links', {
        'tableSelector': '#external-links',
        'structure': [
            {'name': 'name', 'type': 'string'},
            {'name': 'alias', 'type': 'string'}
        ]
    });
    tables['environment'] = new EditableTable('environment', {
        'tableSelector': '#environment',
        'structure': [
            {'name': 'name', 'type': 'string'},
            {'name': 'value', 'type': 'string'}
        ]
    });

    var Container = {
        selectors: {
            container: '.containers',
            editContainer: '.edit-container',
            saveContainer: '.save-container',
            deleteContainer: '.delete-container',
            addContainer: '.add-container',
            containerItem: '.container-item',
            optionsContainer: '.options',
            containerName: '.container-name input'
        },
        /**
         * Id контейнера, который в данный момент редактируется.
         */
        activeContainerId: 0,
        /**
         * Здесь храниться информация со всех контейнеров.
         */
        info: {},
        /**
         * Доступные таблицы их поля в контейнере.
         */
        allowedTables: {
            'ports': ['local_port', 'remote_port'],
            'sections': ['local_path', 'remote_path', 'file'],
            'environment': ['name', 'value'],
            'external_links': ['name', 'alias']
        },
        /**
         * Массив обработчиков кликов.
         */
        handlers: {
            /**
             * Клик по кнопке "добавить контейнер".
             */
            addContainer: function (event) {
                var _this = event.data._this;

                _this.addContainer('Unknown container', _this.getNewContainerId());
            },
            /**
             * Клик по кнопке "редактировать контейнер".
             */
            editContainer: function (event) {
                var _this = event.data._this;
                // если в данный момент не редактируется никакой контейнер
                if (0 === _this.activeContainerId) {
                    var containerId = $(this).parent().parent().data('id');
                    _this.activeContainerId = containerId;
                    // отображаем все опции контейнера
                    _this.displayContainerOptions(containerId);
                    // показываем опции
                    _this.toggleOptions();
                    // добавляем контейнеру кнопку "сохранить"
                    $(this).parent().prepend('<i class="fa fa-check save-container"></i>');
                } else {
                    alert('At first you should save the container which is being edited.');
                }
            },
            /**
             * Клик по кнопке "сохранить контейнер".
             */
            saveContainer: function (event) {
                var _this = event.data._this;

                if (_this.saveContainer()) {
                    // удаляем кнопку "сохранить контейнер"
                    $(this).parent().find(_this.selectors.saveContainer).remove();
                    _this.activeContainerId = 0;
                    // скрываем опции
                    _this.toggleOptions();
                }
            },
            /**
             * Клик по кнопке "удалить контейнер".
             */
            deleteContainer: function (event) {
                var _this = event.data._this;

                var containerId = $(this).parent().parent().data('id');

                delete _this.info[containerId];

                // удаляем контейнер
                $(this).parent().parent().remove();
            }
        },
        /**
         * Возвращает html код для "контейнера".
         * @returns {string}
         */
        generateContainerItem: function (name, id) {
            return '\
                <div class="container-item" data-id="' + (typeof id !== typeof undefined ? id : this.getNewContainerId()) + '">\
                <div class="container-name">\
                <input type="text" title="Container\'s name" value="' + name + '">\
                </div>\
                <div class="container-status">\
                <b class="container-status-down"></b>\
                </div>\
                <div class="container-actions">\
                <i class="fa fa-pencil edit-container" title="Edit container"></i>\
                <i class="fa fa-times delete-container" title="Delete container"></i>\
                <i class="fa fa-plus add-container" title="Add container"></i>\
                </div>\
                </div>';
        },
        /**
         * Возвращает id для нового контейнера.
         * @returns {number}
         */
        getNewContainerId: function () {
            var maxId = 1;
            // перебираем все контейнеры и находим контейнер с самым большим id
            $(this.selectors.container).find(this.selectors.containerItem).each(function (index, container) {
                if (parseInt($(container).data('id')) > maxId) {
                    maxId = $(container).data('id');
                }
            });
            // id для нового контейнера = максимальный id + 1
            return maxId + 1;
        },
        /**
         * Сохраняет опции контейнера, который редактируется, в общий массив info.
         * @returns {boolean}
         */
        saveContainer: function () {

            var _this = this;

            var imageBranch = $(this.selectors.optionsContainer) .find('#image-branch').val();
            var restart = $(this.selectors.optionsContainer) .find('#restart').val();
            var imageType = $(this.selectors.optionsContainer).find('[name=image-type]:checked').val();
            var imageId = $(this.selectors.optionsContainer).find('#image-id').val();
            var name = $(this.selectors.containerItem + '[data-id=' + this.activeContainerId + ']')
                .find(this.selectors.containerName)
                .val();

            this.info[this.activeContainerId]['restart'] = restart;
            this.info[this.activeContainerId]['image_id'] = imageId ? imageId : 0;
            this.info[this.activeContainerId]['image_type'] = imageType ? imageType : <?= \app\models\ApplicationForm::IMAGE_TYPE['local'] ?>;
            this.info[this.activeContainerId]['name'] = name;
            this.info[this.activeContainerId]['image_branch'] = imageBranch;

            // значения из таблиц контейнера
            $.each(this.allowedTables, function (table, columns) {
                _this.info[_this.activeContainerId][table] = tables[table].compileRows();
            });

            return true;
        },
        /**
         * Добавляет контейнер с именем и локальным id.
         */
        addContainer: function (name, id) {
            this.info[id] = {
                '_action': 'create',
                '_id': false,
                'name': name,
                'restart': <?= \app\models\ApplicationContainer::RESTART_ALWAYS ?>,
                'image_id': 0,
                'image_type': <?= \app\models\ApplicationForm::IMAGE_TYPE['local'] ?>,
                'ports': [],
                'sections': [],
                'environment': [],
                'external_links': [],
                'image_branch': 0
            };

            $(this.selectors.container).append(this.generateContainerItem(name, id));
        },
        /**
         * Отображает все опции контейнера.
         * @param id {integer}
         */
        displayContainerOptions: function (id) {
            var _this = this;

            if (this.info[id]) {
                this.cleanTables();
                // перебираем все таблицы контейнера
                $.each(this.allowedTables, function (table, columns) {
                    _this.displayRowsInTable(table, id);
                });

                var imageType = this.info[id]['image_type'];
                var imageBranch = this.info[id]['image_branch'];
                var imageId = this.info[id]['image_id'];
                var restart = this.info[id]['restart'];

                setBranchesForImage(imageId);
//                console.log(imageId);

                $(this.selectors.optionsContainer).find('#restart').find('option[value=' + restart + ']').prop('selected', true);
                $(this.selectors.optionsContainer).find('#image-id').find('option[value=' + imageId + ']').prop('selected', true);
                $(this.selectors.optionsContainer).find('#image-branch').find('option[value=' + imageBranch + ']').prop('selected', true);
                $(this.selectors.optionsContainer).find('[name=image-type][value=' + imageType + ']').prop('checked', true);
            }
        },
        /**
         * Показывает или скрывает контейнер с опциями.
         */
        toggleOptions: function () {
            $(this.selectors.optionsContainer).toggle();
        },
        /**
         * Очищает все таблицы с опциями.
         */
        cleanTables: function () {
            $.each(tables, function (tableName, table) {
                table.deleteAllRows();
            });
        },
        /**
         * Из массива info берет строки указанной таблицы для указанного контейнера и отображает указанную таблицу.
         * @param table {string}
         * @param sectionId {integer}
         */
        displayRowsInTable: function (table, sectionId) {
            for (var i = 0; i < this.info[sectionId][table].length; i++) {
                var row = this.info[sectionId][table][i];
                var data = {_id: row.id};

                // перебираем колонки таблицы чтобы получить их значения
                for (var j = 0; j < this.allowedTables[table].length; j++) {
                    var field = this.allowedTables[table][j];
                    data[field] = row[field];
                }

                tables[table].addRow(data, row._action === 'create');
            }

            tables[table].displayRows();
        },
        /**
         * Возвращает контейнеры и их опции.
         * @returns {Container.info|{}}
         */
        compileContainers: function () {
            return this.info;
        },
        /**
         * Указывает id из БД для новосозданных строк в указанной таблице.
         * Массив updates имеет формат:
         * [
         *      локальный id => id из бд|null,
         *      ...
         * ].
         * @param table {string} имя таблицы
         * @param containerId {integer}
         * @param updates {{}} массив обовлений
         */
        updateTablesRowsIds: function (table, containerId, updates) {
            var _this = this;

            $.each(updates, function (index, id) {
                if (id != null) {
                    // containerLocalId - индекс контейнера в массиве info
                    $.each(_this.info, function (containerLocalId, containerInfo) {
                        if (containerInfo._id == containerId) {
                            if (_this.info[containerLocalId][table][index]) {
                                _this.info[containerLocalId][table][index].id = id;
                                _this.info[containerLocalId][table][index]._action = 'none';
                            }
                        }
                    });
                }
            });
        },
        /**
         * Обновляет id контейнеров, если были созданые новые.
         * Массив updates имеет формат:
         * [
         *      локальный id => id из бд|null,
         *      ...
         * ].
         * Локальный id => индекс строки в массиве строк таблицы.
         * @param updates
         */
        updateContainersIds: function (updates) {
            var _this = this;

            $.each(updates, function (index, id) {
                if (id != null) {
                    if (_this.info[index]) {
                        _this.info[index]._id = id;
                        _this.info[index]._action = 'none';
                    }
                }
            });
        },
        /**
         * Из общего массива info в каждом контейнере удаляет строки с '_action' = 'delete' в каждой таблице.
         */
        cleanDeletedRowsFromTables: function () {
            var _this = this;
            // перебираем все контейнеры
            $.each(this.info, function (containerIndex, container) {
                // перебираем все таблицы, доступые в контейнере
                $.each(_this.allowedTables, function (tableName, tableFields) {
                    if (container[tableName]) {
                        // перебираем все строки таблицы
                        $.each(container[tableName], function (rowIndex, row) {
                            if (row._action === 'delete') {
                                _this.info[containerIndex][tableName].splice(rowIndex, 1);
                            }
                        });
                    }
                });
            });
        }
    };

    // клик по кнопке "добавить контейнер"
    $(body).delegate(Container.selectors.addContainer, 'click', {_this: Container}, Container.handlers.addContainer);
    // клик по кнопке "редактировать контейнер"
    $(body).delegate(Container.selectors.editContainer, 'click', {_this: Container}, Container.handlers.editContainer);
    // клик по кнопке "сохранить контейнер"
    $(body).delegate(Container.selectors.saveContainer, 'click', {_this: Container}, Container.handlers.saveContainer);
    // клик по кнопке "удалить контейнер"
    $(body).delegate(Container.selectors.deleteContainer, 'click', {_this: Container}, Container.handlers.deleteContainer);

    $(function () {

        $.each(tables, function (tableName, table) {
            table.displayRows();
        });

        // перед отправкой формы собираем данные о контейнерах и подставляем в форму
        Form.prototype.beforeSubmit = function () {
            // если еще редактируется какой-то контейнер, тогда не даем отправить форму
            if (Container.activeContainerId !== 0) {
                alert('At first you should finish editing containers.');
                return false;
            }
            return {containers: Container.compileContainers()};
        };

        Form.prototype.afterCreate = function (response, data) {
            $.each(tables, function (tableName, table) {
                table.displayRows();
                table.cleanDeletedRows();
            });

            this.afterSubmit(response, 'create');
        };

        new Form('application', {
            'url': '<?= \yii\helpers\Url::to(['/applications/save']) ?>',
            'fields': ['name'],
            'action': 'create'
        });
    });
</script>