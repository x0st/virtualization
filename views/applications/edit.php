<?php
/**
 * Created by PhpStorm.
 * User: x0st
 * Date: 15.08.16
 * Time: 01:21
 * @var $this \yii\web\View
 * @var $application \app\models\Application
 * @var $images \app\models\Image[]
 */
$this->registerCssFile(\yii\helpers\Url::to(['/css/applications.manage.css']));
?>
<div class="content-header">
    <div class="row">
        <div class="col-md-6 pull-left">
            <h3>
                Application: <?= $application->name ?>
                <a href="#" class="fa fa-refresh btn-restart-application" title="Restart"></a>
                <a href="/applications/clone/<?= $application->id ?>" class="fa fa-clone btn-clone-application" title="Clone"></a>
            </h3>
        </div>
    </div>
</div>
<div data-alert-form></div>
<div class="content">
    <div class="box box-solid">
        <div class="box-body">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" id="name" class="form-control" value="<?= $application->name ?>">
                    </div>
                    <div class="form-group">
                        <label>Containers</label>
                        <div class="containers">
                        </div>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-lg btn-warning btn-update">Update</button>
                    </div>
                </div>
                <div class="col-md-9 options">
                    <div class="form-group">
                        <label for="image-id">Image</label>
                        <select id="image-id" class="form-control">
                            <?php foreach($images as $image): ?>
                                <option value="<?= $image->id ?>"><?= $image->name ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="image-branch">Image branch</label>
                        <select id="image-branch" class="form-control"></select>
                    </div>
                    <div class="form-group">
                        <label>Image type</label>
                        <div class="radio">
                            <label>
                                <input type="radio" name="image-type" value="<?= \app\models\ApplicationForm::IMAGE_TYPE['local'] ?>">
                                Local image
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="image-type" value="<?= \app\models\ApplicationForm::IMAGE_TYPE['remote'] ?>">
                                Remote image
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="image-type" value="<?= \app\models\ApplicationForm::IMAGE_TYPE['dockerfile'] ?>">
                                Dockerfile
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="restart">Restart</label>
                        <select id="restart" class="form-control">
                            <option value="<?= \app\models\ApplicationContainer::RESTART_ALWAYS ?>">Always</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="ports">Ports</label>
                        <table id="ports" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Local port</th>
                                <th>Remote port</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                    <div class="form-group">
                        <label for="links">Links</label>
                        <table id="links" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Container</th>
                                <th>Alias</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                    <div class="form-group">
                        <label for="external-links">External links</label>
                        <table id="external-links" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Alias</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                    <div class="form-group">
                        <label for="sections">Sections</label>
                        <table id="sections" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Local path</th>
                                <th>Remote path</th>
                                <th>File</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                    <div class="form-group">
                        <label for="environment">Environment</label>
                        <table id="environment" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Value</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                    <div class="form-group">
                        <label for="volumes-from">Volumes from</label>
                        <table id="volumes-from" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Container</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                    <div class="form-group">
                        <label for="dependencies">Dependencies</label>
                        <table id="dependencies" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Container</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var branches = {};

    <?php foreach ($images as $image): ?>
        branches[<?= $image->id ?>] = <?= json_encode(array_map(function ($branch) {
            return ['id' => $branch->id, 'name' => $branch->name];
        }, $image->branches)) ?>;
    <?php endforeach; ?>

    /**
     * Устанавливает ветки в select для образа по его id.
     * @param imageId {int}
     */
    function setBranchesForImage(imageId) {
        $('#image-branch').find('option').remove();

        if (branches[imageId]) {
            $.each(branches[imageId], function (index, branch) {
                $('#image-branch').append('<option value="' + branch.id + '">' + branch.name + '</option>');
            });
        }
    }

    // после изменения образа
    // устанавливаем ветки для него
    $('#image-id').change(function () {
        setBranchesForImage($(this).val());
    });

    $('.btn-restart-application').click(restartApplication);

    /**
     * ajax запрос на перезагрузку приложения.
     */
    function restartApplication() {
        $.ajax({
            url: '<?= \yii\helpers\Url::to(['/applications/restart/' . $application->id]) ?>',
            dataType: 'json',
            success: function (response) {
                if (response.status) {
                    alert('The application has been marked to restart it.');
                }
            },
            error: function () {
                alert('Unknown error');
            }
        });
    }

    var body = $('body');
    var tables = {};

    tables['ports'] = new EditableTable('ports', {
        'tableSelector': '#ports',
        'structure': [
            {'name': 'local_port', 'type': 'string'},
            {'name': 'remote_port', 'type': 'string'}
        ]
    });
    tables['links'] = new EditableTable('links', {
        'tableSelector': '#links',
        'structure': [
            {
                'name': 'linked_container_id', 'type': 'select', 'options': [
                <?php foreach ($application->containers as $i => $container): ?>
                {'name': '<?= $container->name ?>', 'value': '<?= $container->id ?>'},
                <?php endforeach; ?>
            ]
            },
            {'name': 'linked_container_alias', 'type': 'string'}
        ]
    });
    tables['external_links'] = new EditableTable('external_links', {
        'tableSelector': '#external-links',
        'structure': [
            {'name': 'name', 'type': 'string'},
            {'name': 'alias', 'type': 'string'}
        ]
    });
    tables['volumes_from'] = new EditableTable('volumes_from', {
        'tableSelector': '#volumes-from',
        'structure': [
            {
                'name': 'linked_container_id', 'type': 'select', 'options': [
                <?php foreach ($application->containers as $i => $container): ?>
                {'name': '<?= $container->name ?>', 'value': '<?= $container->id ?>'},
                <?php endforeach; ?>
            ]
            }
        ]
    });
    tables['dependencies'] = new EditableTable('dependencies', {
        'tableSelector': '#dependencies',
        'structure': [
            {
                'name': 'linked_container_id', 'type': 'select', 'options': [
                <?php foreach ($application->containers as $i => $container): ?>
                {'name': '<?= $container->name ?>', 'value': '<?= $container->id ?>'},
                <?php endforeach; ?>
            ]
            }
        ]
    });
    tables['sections'] = new EditableTable('sections', {
        'tableSelector': '#sections',
        'structure': [
            {'name':'local_path', 'type':'string'},
            {'name':'remote_path', 'type':'string'},
            {
                'name':'file',
                'type':'file',
                // сюда будет загружаться файл
                'url': '<?= \yii\helpers\Url::to(['/applications/upload-archive']) ?>',
                // сообщение об ошибке, если вдруг запрос не дойдет до сервера
                'error-message': 'Error of file uploading',
                // нужно ли показывать ссылку для скачивания
                'download': true,
                // отсюда будет качаться файл
                'download-url': '<?= \yii\helpers\Url::to(['/applications/download-archive/']) ?>',
            }
        ]
    });
    tables['environment'] = new EditableTable('environment', {
        'tableSelector': '#environment',
        'structure': [
            {'name':'name', 'type':'string'},
            {'name':'value', 'type':'string'}
        ]
    });

    var Container = {
        selectors: {
            container: '.containers',
            editContainer: '.edit-container',
            saveContainer: '.save-container',
            deleteContainer: '.delete-container',
            addContainer: '.add-container',
            containerItem: '.container-item',
            optionsContainer: '.options',
            containerName: '.container-name input'
        },
        /**
         * Id контейнера, который в данный момент редактируется.
         */
        activeContainerId: 0,
        /**
         * Здесь храниться информация со всех контейнеров.
         */
        info: {},
        /**
         * Доступные таблицы их поля в контейнере.
         */
        allowedTables: {
            'ports': ['local_port', 'remote_port'],
            'sections': ['local_path', 'remote_path', 'file'],
            'links': ['linked_container_id', 'linked_container_alias'],
            'external_links': ['name', 'alias'],
            'environment': ['name', 'value'],
            'volumes_from': ['linked_container_id'],
            'dependencies': ['linked_container_id']
        },
        /**
         * Массив обработчиков кликов.
         */
        handlers: {
            /**
             * Клик по кнопке "добавить контейнер".
             */
            addContainer: function (event) {
                var _this = event.data._this;

                _this.addContainer('Unknown container', _this.getNewContainerId(), 0, 'create', false, {
                    'ports': [],
                    'sections': [],
                    'links': [],
                    'external_links': [],
                    'volumes_from': [],
                    'environment': [],
                    'restart': <?= \app\models\ApplicationContainer::RESTART_ALWAYS ?>,
                    'image_id': 0,
                    'image_type': <?= \app\models\ApplicationForm::IMAGE_TYPE['local'] ?>
                });
            },
            /**
             * Клик по кнопке "редактировать контейнер".
             */
            editContainer: function (event) {
                var _this = event.data._this;
                // если в данный момент не редактируется никакой контейнер
                if (0 === _this.activeContainerId) {
                    var containerId = $(this).parent().parent().data('id');
                    _this.activeContainerId = containerId;
                    // отображаем все опции контейнера
                    _this.displayContainerOptions(containerId);
                    // показываем опции
                    _this.toggleOptions();
                    // добавляем контейнеру кнопку "сохранить"
                    $(this).parent().prepend('<i class="fa fa-check save-container"></i>');
                } else {
                    alert('At first you should save the container which is being edited.');
                }
            },
            /**
             * Клик по кнопке "сохранить контейнер".
             */
            saveContainer: function (event) {
                var _this = event.data._this;

                if (_this.saveContainer()) {
                    // удаляем кнопку "сохранить контейнер"
                    $(this).parent().find(_this.selectors.saveContainer).remove();
                    _this.activeContainerId = 0;
                    // скрываем опции
                    _this.toggleOptions();
                }
            },
            /**
             * Клик по кнопке "удалить контейнер".
             */
            deleteContainer: function (event) {
                var _this = event.data._this;

                var containerId = $(this).parent().parent().data('id');

                if (_this.info[containerId]) {
                    _this.info[containerId]['_action'] = 'delete';
                }

                // удаляем контейнер
                $(this).parent().parent().remove();
            }
        },
        /**
         * Возвращает html код для "контейнера".
         * @param name {string}
         * @param status {integer} 0|1
         * @param id {integer} локальный id
         * @returns {string}
         */
        generateContainerItem: function (name, status, id) {
            return '\
                <div class="container-item" data-id="' + (typeof id !== typeof undefined ? id : this.getNewContainerId()) + '">\
                <div class="container-name">\
                <input type="text" title="Container\'s name" value="' + name + '">\
                </div>\
                <div class="container-status">\
                <b class="container-status-' + (status == 1 ? 'up' : 'down') + '"></b>\
                </div>\
                <div class="container-actions">\
                <i class="fa fa-pencil edit-container" title="Edit container"></i>\
                <i class="fa fa-times delete-container" title="Delete container"></i>\
                <i class="fa fa-plus add-container" title="Add container"></i>\
                </div>\
                </div>';
        },
        /**
         * Возвращает id для нового контейнера.
         * @returns {number}
         */
        getNewContainerId: function () {
            var maxId = 1;
            // перебираем все контейнеры и находим контейнер с самым большим id
            $(this.selectors.container).find(this.selectors.containerItem).each(function (index, container) {
                if (parseInt($(container).data('id')) > maxId) {
                    maxId = $(container).data('id');
                }
            });
            // id для нового контейнера = максимальный id + 1
            return maxId + 1;
        },
        /**
         * Сохраняет опции контейнера, который редактируется, в общий массив info.
         * @returns {boolean}
         */
        saveContainer: function () {

            var _this = this;


            // значения из таблиц контейнера
            $.each(this.allowedTables, function (table, columns) {
                _this.info[_this.activeContainerId][table] = tables[table].compileRows();
            });

            this.info[this.activeContainerId]['image_branch'] = $(this.selectors.optionsContainer) .find('#image-branch').val();
            this.info[this.activeContainerId]['restart'] = $(this.selectors.optionsContainer) .find('#restart').val();
            this.info[this.activeContainerId]['image_id'] = $(this.selectors.optionsContainer).find('#image-id').val();
            this.info[this.activeContainerId]['image_type'] = $(this.selectors.optionsContainer).find('[name=image-type]:checked').val();
            this.info[this.activeContainerId]['name'] = $(this.selectors.containerItem + '[data-id=' + this.activeContainerId + ']')
                .find(this.selectors.containerName)
                .val();

            // если сохраняется новая строка, не вытащенная из бд, тогда ставим ей action = 'create', иначе 'edit'
            if (this.info[this.activeContainerId]['_action'] && ('none' === this.info[this.activeContainerId]['_action'] || 'edit' === this.info[this.activeContainerId]['_action'])) {
                this.info[this.activeContainerId]['_action'] = 'edit';
            } else {
                this.info[this.activeContainerId]['_action'] = 'create';
            }
            // если сохраняется новая строка, не вытащенная из бд, тогда id из бд false
            if (typeof this.info[this.activeContainerId]['_id'] === typeof undefined) {
                this.info[this.activeContainerId]['_id'] = false;
            }

            // очищаем все таблицы
            this.cleanTables();
            return true;
        },
        /**
         * Добавляет контейнер с именем и локальным id.
         * @param name {string}
         * @param id {integer} локальный id
         * @param status {integer} 1|0
         * @param _action {string} create|none
         * @param _id {integer} id из базы
         * @param options {{}} ассоц. массив опций
         */
        addContainer: function (name, id, status, _action, _id, options) {
            this.info[id] = {
                '_action': _action,
                '_id': _id,
                'name': name
            };

            $.extend(this.info[id], options);

            $(this.selectors.container).append(this.generateContainerItem(name, status, id));
        },
        /**
         * Отображает все опции контейнера.
         * @param id {integer}
         */
        displayContainerOptions: function (id) {
            var _this = this;

            if (this.info[id]) {
                this.cleanTables();
                // отображаем все таблицы контейнера
                $.each(this.allowedTables, function (table, columns) {
                    _this.displayRowsInTable(table, id);
                });

                var imageType = this.info[id]['image_type'];
                var imageId = this.info[id]['image_id'];
                var restart = this.info[id]['restart'];
                var imageBranch = this.info[id]['image_branch'];

                setBranchesForImage(imageId);

                $(this.selectors.optionsContainer).find('#image-branch').find('option[value=' + imageBranch + ']').prop('selected', true);
                $(this.selectors.optionsContainer).find('#restart').find('option[value=' + restart + ']').prop('selected', true);
                $(this.selectors.optionsContainer).find('#image-id').find('option[value=' + imageId + ']').prop('selected', true);
                $(this.selectors.optionsContainer).find('[name=image-type][value=' + imageType + ']').prop('checked', true);
            }
        },
        /**
         * Показывает или скрывает контейнер с опциями.
         */
        toggleOptions: function () {
            $(this.selectors.optionsContainer).toggle();
        },
        /**
         * Очищает все таблицы с опциями.
         */
        cleanTables: function () {
            $.each(tables, function (tableName, table) {
                table.deleteAllRows();
            });
        },
        /**
         * Из массива info берет строки указанной таблицы для указанного контейнера и отображает указанную таблицу.
         * @param table {string}
         * @param sectionId {integer}
         */
        displayRowsInTable: function (table, sectionId) {
            for (var i = 0; i < this.info[sectionId][table].length; i++) {
                var row = this.info[sectionId][table][i];
                var data = {_id: row.id};

                // перебираем колонки таблицы чтобы получить их значения
                for (var j = 0; j < this.allowedTables[table].length; j++) {
                    var field = this.allowedTables[table][j];
                    data[field] = row[field];
                }

                tables[table].addRow(data, row._action === 'create');
            }

            tables[table].displayRows();
        },
        /**
         * Возвращает контейнеры и их опции.
         * @returns {Container.info|{}}
         */
        compileContainers: function () {
            return this.info;
        },
        /**
         * Указывает id из БД для новосозданных строк в указанной таблице.
         * Массив updates имеет формат:
         * [
         *      локальный id => id из бд|null,
         *      ...
         * ].
         * @param table {string} имя таблицы
         * @param containerId {integer}
         * @param updates {{}} массив обовлений
         */
        updateTablesRowsIds: function (table, containerId, updates) {
            var _this = this;

            $.each(updates, function (index, id) {
                if (id != null) {
                    // containerLocalId - индекс контейнера в массиве info
                    $.each(_this.info, function (containerLocalId, containerInfo) {
                        if (containerInfo._id == containerId) {
                            if (_this.info[containerLocalId][table][index]) {
                                _this.info[containerLocalId][table][index].id = id;
                                _this.info[containerLocalId][table][index]._action = 'none';
                            }
                        }
                    });
                }
            });
        },
        /**
         * Обновляет id контейнеров, если были созданые новые.
         * Массив updates имеет формат:
         * [
         *      локальный id => id из бд|null,
         *      ...
         * ].
         * Локальный id => индекс строки в массиве строк таблицы.
         * @param updates
         */
        updateContainersIds: function (updates) {
            var _this = this;

            $.each(updates, function (index, id) {
                if (id != null) {
                    if (_this.info[index]) {
                        _this.info[index]._id = id;
                        _this.info[index]._action = 'none';
                    }
                }
            });
        },
        /**
         * Из общего массива info в каждом контейнере удаляет строки с '_action' = 'delete' в каждой таблице.
         */
        cleanDeletedRowsFromTables: function () {
            var _this = this;
            // перебираем все контейнеры
            $.each(this.info, function (containerIndex, container) {
                // перебираем все таблицы, доступые в контейнере
                $.each(_this.allowedTables, function (tableName, tableFields) {
                    if (container[tableName]) {
                        // перебираем все строки таблицы
                        $.each(container[tableName], function (rowIndex, row) {
                            if (row._action === 'delete') {
                                _this.info[containerIndex][tableName].splice(rowIndex, 1);
                            }
                        });
                    }
                });
            });
        }
    };

    // клик по кнопке "добавить контейнер"
    $(body).delegate(Container.selectors.addContainer, 'click', {_this: Container}, Container.handlers.addContainer);
    // клик по кнопке "редактировать контейнер"
    $(body).delegate(Container.selectors.editContainer, 'click', {_this: Container}, Container.handlers.editContainer);
    // клик по кнопке "сохранить контейнер"
    $(body).delegate(Container.selectors.saveContainer, 'click', {_this: Container}, Container.handlers.saveContainer);
    // клик по кнопке "удалить контейнер"
    $(body).delegate(Container.selectors.deleteContainer, 'click', {_this: Container}, Container.handlers.deleteContainer);

    $(function () {
        // разворачиваем контейнеры из бд
        <?php foreach ($application->containers as $i => $container): ?>
        Container.addContainer('<?= $container->name ?>', <?= $i + 1 ?>, <?= $container->status ?>, 'none', <?= $container->id ?>, {
            'ports': <?= json_encode(array_map(function ($item) {
                    return [
                        'local_port' => $item->local_port,
                        'remote_port' => $item->local_port,
                        'id' => $item->id,
                        '_action' => 'none'
                    ];
                    }, $container->ports)) ?>,
            'links': <?= json_encode(array_map(function ($item) {
                    return [
                        'linked_container_id' => $item->linked_container_id,
                        'linked_container_alias' => $item->linked_container_alias,
                        'id' => $item->id,
                        '_action' => 'none'
                    ];
                    }, $container->links)) ?>,
            'external_links': <?= json_encode(array_map(function ($item) {
                    return [
                        'name' => $item->name,
                        'alias' => $item->alias,
                        'id' => $item->id,
                        '_action' => 'none'
                    ];
                    }, $container->externalLinks)) ?>,
            'sections': <?= json_encode(array_map(function ($item) {
                    return [
                        'local_path' => $item->local_path,
                        'remote_path' => $item->remote_path,
                        'file' => $item->file,
                        'id' => $item->id,
                        '_action' => 'none'
                    ];
                    }, $container->sections)) ?>,
            'environment': <?= json_encode(array_map(function ($item) {
                        return [
                            'name' => $item->name,
                            'value' => $item->value,
                            'id' => $item->id,
                            '_action' => 'none'
                        ];
                    }, $container->environment)) ?>,
            'volumes_from': <?= json_encode(array_map(function ($item) {
                        return [
                            'linked_container_id' => $item->linked_container_id,
                            'id' => $item->id,
                            '_action' => 'none'
                        ];
                    }, $container->volumesFrom)) ?>,
            'dependencies': <?= json_encode(array_map(function ($item) {
                        return [
                            'linked_container_id' => $item->linked_container_id,
                            'id' => $item->id,
                            '_action' => 'none'
                        ];
                    }, $container->volumesFrom)) ?>,
            'image_type': <?= $container->image_type ?>,
            'image_branch': <?= $container->image_branch ?>,
            'image_id': <?= $container->image_id ?>,
            'restart': <?= $container->restart ?>
        });
        <?php endforeach; ?>


        $.each(tables, function (tableName, table) {
            table.displayRows();
        });

        // перед отправкой формы собираем данные о контейнерах и подставляем в форму
        Form.prototype.beforeSubmit = function () {
            // если еще редактируется какой-то контейнер, тогда не даем отправить форму
            if (Container.activeContainerId !== 0) {
                alert('At first you should finish editing containers.');
                return false;
            }
            return {containers: Container.compileContainers()};
        };

        // после submit'а формы обновляем id контейнеров, если те были созданы,
        // а так-же id строк в таблицах контейнеров
        Form.prototype.afterUpdate = function (response, data) {

            if (response.status) {
                Container.cleanDeletedRowsFromTables();

                Container.updateContainersIds(response.containersIds);

                if (response.updates !== null) {
                    // перебор изменений в таблицах контейнеров
                    $.each(response.updates, function (containerId, containerTables) {
                        // перебираем таблицы контейнера
                        $.each(containerTables, function (table, changes) {
                            Container.updateTablesRowsIds(table, containerId, changes);
                        });
                    });
                }

                $.each(tables, function (tableName, table) {
                    table.displayRows();
                    table.cleanDeletedRows();
                });
            }

            this.afterSubmit(response, 'update')
        };

        new Form('application', {
            'url': '<?= \yii\helpers\Url::to(['/applications/save/' . $application->id]) ?>',
            'fields': ['name'],
            'action': 'update'
        });
    });
</script>