<?php
/**
 * Created by PhpStorm.
 * User: x0st
 * Date: 13.08.16
 * Time: 23:55
 */
?>
<div class="content-header">
    <div class="row">
        <div class="col-md-6 pull-left">
            <h3>Applications</h3>
        </div>
        <div class="col-md-6 pull-right">
            <a href="<?= \yii\helpers\Url::to(['/applications/new']) ?>" class="btn btn-warning pull-right">New
                application</a>
        </div>
    </div>
</div>
<div data-alert-list-table></div>
<div class="content">
    <div class="box box-solid">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <?= \app\widgets\ListTable::widget([
                        'id' => 'applications',
                        'columns' => [
                            'id' => ['Id'],
                            'name' => ['Name'],
                            'status' => ['Status'],
                            'actions' => true,
                        ],
                        'pagination' => true
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        (new ListTable('applications', {
            structure: ['id', 'name', 'status'],
            tableSelector: '#applications',
            url: '<?= \yii\helpers\Url::to(['/applications/get-list']) ?>',
            actions: {
                'edit': {ajax: false},
                'log': {ajax: false},
                'delete': {ajax: true}
            }
        })).load();
    });
</script>